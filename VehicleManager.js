// Traffic Module (c) Matti Kuonanoja 2014 
//
// - Contains all Vehicle objects

engine.IncludeFile("TrafficModule/Vehicle.js")

GLOBAL_TOTAL_VEHICLE_COUNT = 0;

var VehicleManager = function(scene) {
    this.scene = scene;
    
    this.averageSpeed = undefined;
    this.vehicles = [];
    
    console.LogInfo(">> VehicleManager: Initialized.");
}

VehicleManager.prototype.getVehicleCount = function() {
    return this.vehicles.length;
}

VehicleManager.prototype.getAverageSpeed = function() {
    return this.averageSpeed;
}

VehicleManager.prototype.getAverageRouteLength = function() {
    if (this.vehicles.length == 0) {
        return undefined;
    }
    
    var routeSum = 0;
    for (var i = 0; i < this.vehicles.length; i++) {
        var vehicle = this.vehicles[i];
        routeSum += vehicle.route.getLength();
    }
    var averageRouteLength = routeSum / this.vehicles.length;
    return averageRouteLength; 
}

VehicleManager.prototype.getVehicleByEntityObjectId = function(id) {
    for (var i = 0; i < this.vehicles.length; i++) {
        var vehicle = this.vehicles[i];
        if (vehicle.vehicleEntity.entityObject.id == id) {
            return vehicle;
        }
    }
    return undefined;
}

VehicleManager.prototype.update = function(deltaTime) {
    profiler.BeginBlock("VehicleManager update");
    for (var i = 0; i < this.vehicles.length; i++) {
        var vehicle = this.vehicles[i];
        this.vehicles[i].update(deltaTime);
    }
    this.__updateAverageSpeed();    
    profiler.EndBlock("VehicleManager update");
}

VehicleManager.prototype.__updateAverageSpeed = function() {
    var speedSum = 0;
    var movingVehicleCount = 0
    for (var i = 0; i < this.vehicles.length; i++) {
        var vehicle = this.vehicles[i];
        if (!vehicle.isPaused()) {
            speedSum += vehicle.getSpeed();
            movingVehicleCount += 1;
        }
    }
    if (movingVehicleCount > 0) {
        this.averageSpeed = speedSum / movingVehicleCount;
    } else {
        this.averageSpeed = undefined;
    }
}

VehicleManager.prototype.createVehicle = function(route, currentTime) {
    var vehicle = new Vehicle(this.scene, route, currentTime);
    this.vehicles.push(vehicle);
    var routeId = vehicle.route.id;
    GLOBAL_TOTAL_VEHICLE_COUNT += 1;
}

VehicleManager.prototype.removeFinishedVehicles = function(simulationTime) {
    profiler.BeginBlock("removeFinishedVehicles");
    var finishedVehicles = []
    for(var i = 0; i < this.vehicles.length; i++) {
        var vehicle = this.vehicles[i];
        if (vehicle.isRouteCompleted || !vehicle.route.existAt(simulationTime)) {
            finishedVehicles.push(vehicle)
        }
    }
    for (var i = 0; i < finishedVehicles.length; i++) {
        var vehicle = finishedVehicles[i];
        this.removeVehicle(vehicle);
    }
    profiler.EndBlock("removeFinishedVehicles");
}

VehicleManager.prototype.removeAllVehicles = function() {
    for (var i = 0; i < this.vehicles.length; i++) {
        var vehicle = this.vehicles[i];
        vehicle.removeEntityFromScene();
    }
    this.vehicles = [];
    GLOBAL_TOTAL_VEHICLE_COUNT = 0;
}

VehicleManager.prototype.removeVehicle = function(vehicle) {
    vehicle.removeEntityFromScene();
    var index = this.vehicles.indexOf(vehicle);
    this.vehicles.splice(index,1);
    
    GLOBAL_TOTAL_VEHICLE_COUNT -= 1;
}
