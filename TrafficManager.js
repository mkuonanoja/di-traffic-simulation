// (c) Matti Kuonanoja 2014 

function TrafficManager(trafficDatabase) {
    this.trafficDatabase = trafficDatabase;
    
    this.nextRouteIndex = 0;
    this.isOutOfRoutes = false;
    
    console.LogInfo(">> TrafficManager: Initialized");    
}

TrafficManager.prototype.isOutOfRoutes = function(timestamp) {
    return this.isOutOfRoutes;
}

TrafficManager.prototype.moveToFirstUnfinishedRoute = function(timestamp) {
    this.isOutOfRoutes = false;
    for (var i = 0; i < this.trafficDatabase.routes.length; i++) {
        var route = this.trafficDatabase.getRouteByIndex(i);
        if (route.existAt(timestamp)) {
            this.nextRouteIndex = i;
            console.LogInfo(">> TrafficManager: set index to "+i+" "+route.startTime+" - "+route.endTime);
            return;
        }
    }
    console.LogInfo(">> TrafficManager: out of routes");
    this.isOutOfRoutes = true;
}

TrafficManager.prototype.getNewActiveRoutes = function(timestamp) {
    if (this.isOutOfRoutes) {
        return [];
    }
    var newActiveRoutes = [];    
    var route = this.getRouteByCurrentIndex();
     
    while (route.existAt(timestamp) || route.isEndedBy(timestamp)) {
        if (route.existAt(timestamp)) {
            newActiveRoutes.push(route);
        } 
        this.increaseRouteIndex();
        if (this.isOutOfRoutes) {
            break;
        }
        route = this.getRouteByCurrentIndex();
    }
    return newActiveRoutes;
}

TrafficManager.prototype.getRouteByCurrentIndex = function() {
    return this.trafficDatabase.getRouteByIndex(this.nextRouteIndex);
}

TrafficManager.prototype.increaseRouteIndex = function() {
    if (this.nextRouteIndex < this.trafficDatabase.getRouteCount() - 1) {
        this.nextRouteIndex += 1;  
    } else {
        this.isOutOfRoutes = true;
        console.LogInfo(">> TrafficManager: no more routes");
    }
}
