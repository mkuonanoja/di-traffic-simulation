// (c) Matti Kuonanoja 2014 

engine.IncludeFile("TrafficModule/MathTools.js");
engine.IncludeFile("TrafficModule/Location.js");

var TIMESTAMP_INDEX = 0;
var LATITUDE_INDEX = 1;
var LONGITUDE_INDEX = 2;
var PAUSETIME_INDEX = 3;

function Route(id, waypoints) {
    this.id = id;
    this.waypoints = waypoints;
    
    this.startTime = undefined;
    this.endTime = undefined;
    this.totalLength = undefined;
    
    this.__analyzeRoute();
}

Route.prototype.getWaypointCount = function() {
    return this.waypoints.length;
}

Route.prototype.getTimestamp = function(waypointIndex) {
    return this.waypoints[waypointIndex][TIMESTAMP_INDEX];
}

Route.prototype.getPauseTime = function(waypointIndex) {
    if (waypointIndex >= this.waypoints.length) {
        return;
    }
    if (this.waypoints[waypointIndex].length > PAUSETIME_INDEX) {
        return this.waypoints[waypointIndex][PAUSETIME_INDEX];
    } else {
        return 0;
    }
}

Route.prototype.existAt = function(timestamp) {
    if (this.startTime <= timestamp && this.endTime > timestamp) {
        return true;
    } else {
        return false;
    }
}

Route.prototype.isEndedBy = function(timestamp) {
    if (this.endTime < timestamp) {
        return true;
    } else {
        return false;
    }
}

Route.prototype.getFirstTimestamp = function() {
    return this.getTimestamp(0);
}

Route.prototype.getLastTimestamp = function() {
    return this.getTimestamp(this.waypoints.length-1);
}

Route.prototype.getLatitude = function(waypointIndex) {
    return this.waypoints[waypointIndex][LATITUDE_INDEX];
}

Route.prototype.getLongitude = function(waypointIndex) {
    return this.waypoints[waypointIndex][LONGITUDE_INDEX];
}

Route.prototype.getLocation = function(waypointIndex) {
    var location = new Location(this.getLatitude(waypointIndex), this.getLongitude(waypointIndex));
    return location;
}

Route.prototype.getLength = function() {
    return this.totalLength;
}

Route.prototype.__analyzeRoute = function() {
    var i;
    this.totalLength = 0;
    for (i = 0; i < this.waypoints.length; i++) {
        if (i > 0) {
            var locationA = new Location(this.getLatitude(i-1), this.getLongitude(i-1));
            var locationB = new Location(this.getLatitude(i), this.getLongitude(i));
            var positionA = locationA.getSimulationPosition();
            var positionB = locationB.getSimulationPosition();
            var segmentLength = distanceTo(positionA.x, positionA.y, positionB.x, positionB.y)
            this.totalLength += segmentLength;
            
        }
    }
     
    this.startTime = this.getFirstTimestamp();
    this.endTime = this.getLastTimestamp();
}