// (c) Matti Kuonanoja 2014 

engine.IncludeFile("TrafficModule/MathTools.js")

function Location(latitude, longitude) {
    this.latitude = latitude;
    this.longitude = longitude;
}

Location.prototype.getSimulationPosition = function() {
    return locationToSimulationPosition(this.latitude, this.longitude);
}