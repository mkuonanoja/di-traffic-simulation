// (c) Matti Kuonanoja 2014 
//
// Updates data shown by userinterface and handles element callbacks.

engine.ImportExtension("qt.core");
engine.ImportExtension("qt.gui");

var TOP_HEADER_HEIGHT = 60;
var SLIDER_MAX_VALUE = 1000;

// rename to UserInterface

function UserInterface() {
    this.callbackForSetTime = undefined;
    this.callbackForToggleSimulationState = undefined;
    this.callbackForTransparentInfra = undefined;
    this.callbackForFlatInfra = undefined;
    this.callbackForBigVehicles = undefined;
    this.callbackForExhaustEffect = undefined;
    this.callbackForShowIntersections = undefined;
    this.callbackForSetSpeed = undefined;
    this.callbackForNightMode = undefined;
    this.callbackForTrailEffect = undefined;
    this.callbackForAudio = undefined;
    this.callbackGlowEffect = undefined;

    this.topHeaderWidget = undefined;
    this.topHeaderProxyWidget = undefined;

    this.bottomBarWidget = undefined;
    this.bottomBarProxyWidget = undefined;
    
    this.simulationInfoWidget = undefined;
    this.simulationInfoProxyWidget = undefined;

    this.vehicleInfoWidget = undefined;
    this.vehicleInfoProxyWidget = undefined;
    
    this.simulationStateInfo = {
        totalSimulationTime: undefined,
        totalRouteCount: undefined,
        totalRouteLength: undefined,
        currentVehicleCount: undefined,
        currentAverageSpeed: undefined,
        currentAverageRouteLength: undefined
    }
    
    this.vehicleStateInfo = {
        id: undefined,
        type: undefined,
        routeLength: undefined,
        waypointCount: undefined,
        speed: undefined,
        routeLeft: undefined,
        waypointsLeft: undefined
    }    
    
    ui.GraphicsScene().sceneRectChanged.connect(this, this.updateProxyWidgetPositions);
    
    //this.setupMenu();
    this.initTopHeader();
    this.initBottomBar();
    this.initSimulationInfo();
    this.initVehicleInfo();
    
    this.updateProxyWidgetPositions();
}

UserInterface.prototype.setSimulationStateInfo = function(stateInfo) {
    this.simulationStateInfo = stateInfo;
    
    var totalSimulationTimeLabel = findChild(this.simulationInfoWidget, "totalSimulationTimeLabel");    
    if (totalSimulationTimeLabel) {
        if (this.simulationStateInfo.totalSimulationTime) {
            var hours = Math.round(this.simulationStateInfo.totalSimulationTime / 60 / 60);
            var minutes = Math.round(this.simulationStateInfo.totalSimulationTime / 60) % 60;
            if (hours == 0) {
                totalSimulationTimeLabel.setText(minutes + " min");    
            } else { 
                totalSimulationTimeLabel.setText(hours + " hours " + minutes + " min");                
            }
        } else {
            totalSimulationTimeLabel.setText("---");    
        }
    } else {
        console.LogInfo(">> UserInterface: Cannot find totalSimulationTimeLabel object");
    }       
    
    var totalRouteCountLabel = findChild(this.simulationInfoWidget, "totalRouteCountLabel");    
    if (totalRouteCountLabel) {
        if (this.simulationStateInfo.totalRouteCount) {
            totalRouteCountLabel.setText(this.simulationStateInfo.totalRouteCount);                
        } else {
            totalRouteCountLabel.setText("---");    
        }
    } else {
        console.LogInfo(">> UserInterface: Cannot find totalSimulationTimeLabel object");
    }   
    
    var totalRouteLengthLabel = findChild(this.simulationInfoWidget, "totalRouteLengthLabel");    
    if (totalRouteLengthLabel) {
        if (this.simulationStateInfo.totalRouteLength) {
            var kilometers = this.simulationStateInfo.totalRouteLength/1000;
            totalRouteLengthLabel.setText(kilometers.toFixed(1) + " km");                
        } else {
            totalRouteLengthLabel.setText("---");    
        }
    } else {
        console.LogInfo(">> UserInterface: Cannot find totalSimulationTimeLabel object");
    }   
    
    var currentVehicleCountLabel = findChild(this.simulationInfoWidget, "currentVehicleCountLabel");    
    if (currentVehicleCountLabel) {
        if (this.simulationStateInfo.currentVehicleCount) {
            currentVehicleCountLabel.setText(this.simulationStateInfo.currentVehicleCount);
        } else {
            currentVehicleCountLabel.setText("---");    
        }
    } else {
        console.LogInfo(">> UserInterface: Cannot find totalSimulationTimeLabel object");
    }       
    
    var averageSpeedLabel = findChild(this.simulationInfoWidget, "averageSpeedLabel");    
    if (averageSpeedLabel) {
        if (this.simulationStateInfo.currentAverageSpeed) {
            var speedInKmPerHour = this.simulationStateInfo.currentAverageSpeed * 3.6;
            averageSpeedLabel.setText(speedInKmPerHour.toFixed(1)+" km/h");    
        } else {
            averageSpeedLabel.setText("---");    
        }
    } 

    var averageRouteLengthLabel = findChild(this.simulationInfoWidget, "averageRouteLengthLabel");    
    if (averageRouteLengthLabel) {
        if (this.simulationStateInfo.currentAverageRouteLength) {
            var meters = this.simulationStateInfo.currentAverageRouteLength;
            var kilometers = this.simulationStateInfo.currentAverageRouteLength/1000;
            if (meters < 1000) {
                averageRouteLengthLabel.setText(meters.toFixed(1) + " m");                
            } else {
                averageRouteLengthLabel.setText(kilometers.toFixed(1) + " km");                
            }
        } else {
            averageRouteLengthLabel.setText("---");    
        }
    } 
  
}

UserInterface.prototype.setVehicleStateInfo = function(stateInfo) {
    this.vehicleStateInfo = stateInfo;
    
    if (!this.vehicleStateInfo.id) {
        this.vehicleInfoProxyWidget.visible = false;
    } else {
        this.vehicleInfoProxyWidget.visible = true;
    }
    
    var idLabel = findChild(this.vehicleInfoWidget, "idLabel");    
    if (idLabel) {
        if (this.vehicleStateInfo.id) {
            idLabel.setText(this.vehicleStateInfo.id);
        } else {
            idLabel.setText("---");    
        }
    } 
    
    var typeLabel = findChild(this.vehicleInfoWidget, "typeLabel");    
    if (typeLabel) {
        if (this.vehicleStateInfo.type) {
            typeLabel.setText(this.vehicleStateInfo.type);
        } else {
            typeLabel.setText("---");    
        }
    } 

    var totalRouteLengthLabel = findChild(this.vehicleInfoWidget, "totalRouteLengthLabel");    
    if (totalRouteLengthLabel) {
        if (this.vehicleStateInfo.routeLength) {
            var meters = this.vehicleStateInfo.routeLength;
            var kilometers = this.vehicleStateInfo.routeLength/1000;
            if (meters < 1000) {
                totalRouteLengthLabel.setText(meters.toFixed(1) + " m");                
            } else {
                totalRouteLengthLabel.setText(kilometers.toFixed(1) + " km");                
            }
        } else {
            totalRouteLengthLabel.setText("---");    
        }
    } 

    var waypointCountLabel = findChild(this.vehicleInfoWidget, "waypointCountLabel");    
    if (waypointCountLabel) {
        if (this.vehicleStateInfo.waypointCount) {
            waypointCountLabel.setText(this.vehicleStateInfo.waypointCount);
        } else {
            waypointCountLabel.setText("---");    
        }
    } 

    var speedLabel = findChild(this.vehicleInfoWidget, "speedLabel");    
    if (speedLabel) {
        if (this.vehicleStateInfo.speed) {
            var speedInKmPerHour = this.vehicleStateInfo.speed * 3.6;
            speedLabel.setText(speedInKmPerHour.toFixed(1)+" km/h");            
        } else {
            speedLabel.setText("---");    
        }
    } 

    var routeLeftLabel = findChild(this.vehicleInfoWidget, "routeLeftLabel");    
    if (routeLeftLabel) {
        if (this.vehicleStateInfo.routeLeft) {
            var meters = this.vehicleStateInfo.routeLeft;
            var kilometers = this.vehicleStateInfo.routeLeft/1000;
            if (meters < 1000) {
                routeLeftLabel.setText(meters.toFixed(1) + " m");                
            } else {
                routeLeftLabel.setText(kilometers.toFixed(1) + " km");                
            }
        } else {
            routeLeftLabel.setText("---");    
        }
    } 
    
    var waypointsLeftLabel = findChild(this.vehicleInfoWidget, "waypointsLeftLabel");    
    if (waypointsLeftLabel) {
        if (this.vehicleStateInfo.waypointsLeft) {
                waypointsLeftLabel.setText(this.vehicleStateInfo.waypointsLeft);                
        } else {
            waypointsLeftLabel.setText("---");    
        }
    }     

}

UserInterface.prototype.setSetTimeCallback = function(cb) {
    this.callbackForSetTime = cb;
}

UserInterface.prototype.setToggleSimulationStateCallback = function(cb) {
    this.callbackForToggleSimulationState = cb;
}

UserInterface.prototype.setTransparentInfraCallback = function(cb) {
    this.callbackForTransparentInfra = cb;
}

UserInterface.prototype.setFlatInfraCallback = function(cb) {
    this.callbackForFlatInfra = cb;
}

UserInterface.prototype.setBigVehiclesCallback = function(cb) {
    this.callbackForBigVehicles = cb;
}

UserInterface.prototype.setExhaustEffectCallback = function(cb) {
    this.callbackForExhaustEffect = cb;
}

UserInterface.prototype.setShowIntersectionsCallback = function(cb) {
    this.callbackForShowIntersections = cb;
}

UserInterface.prototype.setSpeedCallback = function(cb) {
    this.callbackForSetSpeed = cb;
}

UserInterface.prototype.setNightModeCallback = function(cb) {
    this.callbackForNightMode = cb;
}

UserInterface.prototype.setTrailEffectCallback = function(cb) {
    this.callbackForTrailEffect = cb;
}

UserInterface.prototype.setAudioCallback = function(cb) {
    this.callbackForAudio = cb;
}

UserInterface.prototype.setGlowEffectCallback = function(cb) {
    this.callbackGlowEffect = cb;
}

UserInterface.prototype.setSimulationTimeFrame = function(startTimestamp, endTimestamp) {
    this.simulationStartTimestamp = startTimestamp;
    this.simulationEndTimestamp = endTimestamp;
}

UserInterface.prototype.updateSimulationTime = function(timestamp, internalCall) {
    var timeSlider = findChild(this.topHeaderWidget, "timeSlider");    
    if (internalCall || !timeSlider.sliderDown) {
        var simulationDateTime = new QDateTime( QDateTime.fromTime_t(timestamp*1) );
        var timeText = simulationDateTime.time().toString();
        var dateText = simulationDateTime.toString("yyyy-MM-d");
        
        var timeElement = findChild(this.topHeaderWidget, "timeLabel");    
        timeElement.setText(timeText);
        
        var dateElement = findChild(this.topHeaderWidget, "dateLabel");    
        dateElement.setText(dateText);
        
        this.updateTimeSlider(timestamp);
    
        // slider position text
        var timeSlider = findChild(this.topHeaderWidget, "timeSlider");    
        var progress = 100 * timeSlider.value / timeSlider.maximum;
        var progresText = "" +progress + " %";
        var sliderProgressLabel = findChild(this.topHeaderWidget, "sliderProgressLabel");    
        sliderProgressLabel.setText(progresText);
    }
}

UserInterface.prototype.updateTimeSlider = function(timestamp) {
    var totalTime = this.simulationEndTimestamp - this.simulationStartTimestamp;
    if (totalTime == 0) {
        return;
    }
    var progress = (timestamp - this.simulationStartTimestamp) / totalTime;
    var timeSlider = findChild(this.topHeaderWidget, "timeSlider");    
    if (timeSlider && !timeSlider.sliderDown) {
        timeSlider.value = SLIDER_MAX_VALUE*progress;
    }
}

UserInterface.prototype.updateSimulationState = function(simulationRunning, simulationSpeed) {
    var stateElement = findChild(this.topHeaderWidget, "simulationStateLabel");    
    var simulationSpeedText = "x " + simulationSpeed;
    
    if (simulationRunning) {
        stateElement.setText("Running ("+simulationSpeedText+")");
    } else {
        stateElement.setText("Paused ("+simulationSpeedText+")");
    }
}

UserInterface.prototype.initSimulationInfo = function() {
    // create QWidget intance
    var addToScene = false;
    var parentWidget = 0;
    this.simulationInfoWidget = ui.LoadFromFile("./TrafficModule/ui/SimulationInfo.ui", addToScene, parentWidget);    
    this.simulationInfoWidget.setWindowState(0x00000004); // Qt::WindowFullScreen
    
    // create QProxyWidgetInstance
    this.simulationInfoProxyWidget = ui.AddWidgetToScene(this.simulationInfoWidget);
    this.simulationInfoProxyWidget.x = 0;
    this.simulationInfoProxyWidget.y = 0;
    this.simulationInfoProxyWidget.visible = true; // !!
    this.simulationInfoProxyWidget.windowFlags = 0; //Qt::WindowFullScreen; // Qt::Widget
}

UserInterface.prototype.initVehicleInfo = function() {
    // create QWidget intance
    var addToScene = false;
    var parentWidget = 0;
    this.vehicleInfoWidget = ui.LoadFromFile("./TrafficModule/ui/VehicleInfo.ui", addToScene, parentWidget);    
    this.vehicleInfoWidget.setWindowState(0x00000004); // Qt::WindowFullScreen
    
    // create QProxyWidgetInstance
    this.vehicleInfoProxyWidget = ui.AddWidgetToScene(this.vehicleInfoWidget);
    this.vehicleInfoProxyWidget.x = 0;
    this.vehicleInfoProxyWidget.y = 0;
    this.vehicleInfoProxyWidget.visible = true; // !!
    this.vehicleInfoProxyWidget.windowFlags = 0; //Qt::WindowFullScreen; // Qt::Widget
}


UserInterface.prototype.initBottomBar = function() {
    // create QWidget intance
    var addToScene = false;
    var parentWidget = 0;
    this.bottomBarWidget = ui.LoadFromFile("./TrafficModule/ui/BottomBar.ui", addToScene, parentWidget);    
    this.bottomBarWidget.setWindowState(0x00000004); // Qt::WindowFullScreen
    
    // create QProxyWidgetInstance
    this.bottomBarProxyWidget = ui.AddWidgetToScene(this.bottomBarWidget);
    this.bottomBarProxyWidget.x = 0;
    this.bottomBarProxyWidget.y = 0;
    this.bottomBarProxyWidget.visible = true; // !!
    this.bottomBarProxyWidget.windowFlags = 0; //Qt::WindowFullScreen; // Qt::Widget
    
    // Flat infra button
    var self = this;
    var flatInfraCheckBox = findChild(this.bottomBarWidget, "flatInfraCheckBox");    
    if (flatInfraCheckBox) {
        flatInfraCheckBox.stateChanged.connect(function(checked) {
            if (checked) { //.if (self.callbackForToggleSimulationState) { self.callbackForToggleSimulationState();}
                if (self.callbackForFlatInfra) {
                    self.callbackForFlatInfra(true);
                }
            } else {
                if (self.callbackForFlatInfra) {
                    self.callbackForFlatInfra(false);
                }
            }
        });
    } 
    
    // transparent infra button
    var self = this;
    var transparentInfraCheckBox = findChild(this.bottomBarWidget, "transparentInfraCheckBox");    
    if (transparentInfraCheckBox) {
        transparentInfraCheckBox.stateChanged.connect(function(checked) {
            if (checked) { //.if (self.callbackForToggleSimulationState) { self.callbackForToggleSimulationState();}
                if (self.callbackForTransparentInfra) {
                    self.callbackForTransparentInfra(true);
                }
            } else {
                if (self.callbackForTransparentInfra) {
                    self.callbackForTransparentInfra(false);
                }
            }
        });
    }     
    
    // Big Vehicles button
    var self = this;
    var bigVehiclesCheckBox = findChild(this.bottomBarWidget, "bigVehiclesCheckBox");    
    if (bigVehiclesCheckBox) {
        bigVehiclesCheckBox.stateChanged.connect(function(checked) {
            if (checked) { //.if (self.callbackForToggleSimulationState) { self.callbackForToggleSimulationState();}
                if (self.callbackForBigVehicles) {
                    self.callbackForBigVehicles(true);
                }
            } else {
                if (self.callbackForBigVehicles) {
                    self.callbackForBigVehicles(false);
                }
            }
        });
    }     
    
    // Exhaust effect button
    var self = this;
    var exhaustEffectCheckBox = findChild(this.bottomBarWidget, "exhaustEffectCheckBox");    
    if (exhaustEffectCheckBox) {
        exhaustEffectCheckBox.stateChanged.connect(function(checked) {
            if (checked) { //.if (self.callbackForToggleSimulationState) { self.callbackForToggleSimulationState();}
                if (self.callbackForExhaustEffect) {
                    self.callbackForExhaustEffect(true);
                }
            } else {
                if (self.callbackForExhaustEffect) {
                    self.callbackForExhaustEffect(false);
                }
            }
        });
    }     
        
    // Exhaust effect button
    var self = this;
    var showIntersectionsCheckBox = findChild(this.bottomBarWidget, "showIntersectionsCheckBox");    
    if (showIntersectionsCheckBox) {
        showIntersectionsCheckBox.stateChanged.connect(function(checked) {
            if (checked) { //.if (self.callbackForToggleSimulationState) { self.callbackForToggleSimulationState();}
                if (self.callbackForShowIntersections) {
                    self.callbackForShowIntersections(true);
                }
            } else {
                if (self.callbackForShowIntersections) {
                    self.callbackForShowIntersections(false);
                }
            }
        });
    }       
    
    // Night mode
    var self = this;
    var nightModeCheckBox = findChild(this.bottomBarWidget, "nightModeCheckBox");    
    if (nightModeCheckBox) {
        nightModeCheckBox.stateChanged.connect(function(checked) {
            if (checked) { //.if (self.callbackForToggleSimulationState) { self.callbackForToggleSimulationState();}
                if (self.callbackForNightMode) {
                    self.callbackForNightMode(true);
                }
            } else {
                if (self.callbackForNightMode) {
                    self.callbackForNightMode(false);
                }
            }
        });
    }       
    
    // Trail effect mode
    var self = this;
    var trailEffectCheckBox = findChild(this.bottomBarWidget, "trailEffectCheckBox");    
    if (trailEffectCheckBox) {
        trailEffectCheckBox.stateChanged.connect(function(checked) {
            if (checked) { //.if (self.callbackForToggleSimulationState) { self.callbackForToggleSimulationState();}
                if (self.callbackForTrailEffect) {
                    self.callbackForTrailEffect(true);
                }
            } else {
                if (self.callbackForTrailEffect) {
                    self.callbackForTrailEffect(false);
                }
            }
        });
    }       

    // Trail effect mode
    var self = this;
    var audioCheckBox = findChild(this.bottomBarWidget, "audioCheckBox");    
    if (audioCheckBox) {
        audioCheckBox.stateChanged.connect(function(checked) {
            if (checked) { //.if (self.callbackForToggleSimulationState) { self.callbackForToggleSimulationState();}
                if (self.callbackForAudio) {
                    self.callbackForAudio(true);
                }
            } else {
                if (self.callbackForAudio) {
                    self.callbackForAudio(false);
                }
            }
        });
    }       
    
    // Glow effect mode
    var self = this;
    var glowCheckBox = findChild(this.bottomBarWidget, "glowCheckBox");    
    if (glowCheckBox) {
        glowCheckBox.stateChanged.connect(function(checked) {
            if (checked) { //.if (self.callbackForToggleSimulationState) { self.callbackForToggleSimulationState();}
                if (self.callbackGlowEffect) {
                    self.callbackGlowEffect(true);
                }
            } else {
                if (self.callbackGlowEffect) {
                    self.callbackGlowEffect(false);
                }
            }
        });
    }         
    
}

UserInterface.prototype.initTopHeader = function() {
    // create QWidget intance
    var addToScene = false;
    var parentWidget = 0;
    this.topHeaderWidget = ui.LoadFromFile("./TrafficModule/ui/TopHeader.ui", addToScene, parentWidget);    
    this.topHeaderWidget.setWindowState(0x00000004); // Qt::WindowFullScreen
    
    // create QProxyWidgetInstance
    this.topHeaderProxyWidget = ui.AddWidgetToScene(this.topHeaderWidget);
    this.topHeaderProxyWidget.x = 0;
    this.topHeaderProxyWidget.y = 0;
    this.topHeaderProxyWidget.visible = true; // !!
    this.topHeaderProxyWidget.windowFlags = 0; //Qt::WindowFullScreen; // Qt::Widget
    
    // Simulation state button init
    var self = this;
    var simulationStateButton = findChild(this.topHeaderWidget, "simulationStateButton");    
    if (simulationStateButton) {
        simulationStateButton.clicked.connect(function() {
            if (self.callbackForToggleSimulationState) { self.callbackForToggleSimulationState();}
        });
    }
    
    // Speed buttons
    var speedButton1 = findChild(this.topHeaderWidget, "speedButton1");
    if (speedButton1) {
        speedButton1.clicked.connect(function() {
            if (self.callbackForSetSpeed) { self.callbackForSetSpeed(0.5);}
        });        
    }
    var speedButton2 = findChild(this.topHeaderWidget, "speedButton2");
    if (speedButton2) {
        speedButton2.clicked.connect(function() {
            if (self.callbackForSetSpeed) { self.callbackForSetSpeed(1);}
        });        
    }
    var speedButton3 = findChild(this.topHeaderWidget, "speedButton3");
    if (speedButton3) {
        speedButton3.clicked.connect(function() {
            if (self.callbackForSetSpeed) {
                self.callbackForSetSpeed(2);
            }
        });        
    }
    var speedButton4 = findChild(this.topHeaderWidget, "speedButton4");
    if (speedButton4) {
        speedButton4.clicked.connect(function() {
            if (self.callbackForSetSpeed) { self.callbackForSetSpeed(5);}
        });        
    }
    var speedButton5 = findChild(this.topHeaderWidget, "speedButton5");
    if (speedButton5) {
        speedButton5.clicked.connect(function() {
            if (self.callbackForSetSpeed) { self.callbackForSetSpeed(10);}
        });        
    }
    
    // Time slider init
    var timeSlider = findChild(this.topHeaderWidget, "timeSlider");    
    if (timeSlider) {
        timeSlider.sliderReleased.connect(function() {
                var newSimulationTime = self.timeSliderPositionToTimestamp(timeSlider.value); //this.sliderValueToTimestamp(sliderElement.value);
                if (self.callbackForSetTime) {
                    self.callbackForSetTime(newSimulationTime);
                }
            }
        );
        timeSlider.sliderMoved.connect(function() {
                var newSimulationTime = self.timeSliderPositionToTimestamp(timeSlider.value); //this.sliderValueToTimestamp(sliderElement.value);
                //var newSimulationDateTime = new QDateTime( QDateTime.fromTime_t(newSimulationTime*1) );
                console.LogInfo(newSimulationTime);
                self.updateSimulationTime(newSimulationTime, true);
            }
        );
    }
}

UserInterface.prototype.timeSliderPositionToTimestamp = function(sliderValue) {

    var simulationTotalTime = this.simulationEndTimestamp - this.simulationStartTimestamp;
    var simulationTime = this.simulationStartTimestamp + (simulationTotalTime * sliderValue) / SLIDER_MAX_VALUE;
    return simulationTime;
}

UserInterface.prototype.updateProxyWidgetPositions = function() {
    var mainWindowRect = ui.GraphicsScene().sceneRect;
    
    if (this.topHeaderProxyWidget) {
        var height = this.topHeaderProxyWidget.size.height();
        this.topHeaderProxyWidget.setGeometry(mainWindowRect.x(), mainWindowRect.y(), mainWindowRect.width(), height);
    }
    
    if (this.bottomBarProxyWidget) {
        var height = this.bottomBarProxyWidget.size.height();
        var posY = mainWindowRect.y()+mainWindowRect.height() - this.bottomBarProxyWidget.size.height();
        this.bottomBarProxyWidget.setGeometry(mainWindowRect.x(), posY , mainWindowRect.width(), height);
    }
    
    if (this.simulationInfoProxyWidget) {
        var MARGINAL = 5;
        var height = this.simulationInfoProxyWidget.size.height();
        var width = this.simulationInfoProxyWidget.size.width();
        var topHeaderHeight = this.topHeaderProxyWidget.size.height();
        var posY = mainWindowRect.y() + topHeaderHeight + MARGINAL;
        var posX = mainWindowRect.x() + MARGINAL;
        this.simulationInfoProxyWidget.setGeometry(posX, posY , width, height);
    }
    
    if (this.vehicleInfoProxyWidget) {
        var MARGINAL = 5;
        var height = this.vehicleInfoProxyWidget.size.height();
        var width = this.vehicleInfoProxyWidget.size.width();
        var bottomBarHeight = this.bottomBarProxyWidget.size.height();
        var posY = mainWindowRect.y() + mainWindowRect.height() - height - bottomBarHeight - MARGINAL;
        var posX = mainWindowRect.x() + MARGINAL;
        this.vehicleInfoProxyWidget.setGeometry(posX, posY , width, height);
    }    
}

UserInterface.prototype.initMenu = function() {
    var menu = ui.MainWindow().menuBar();
    menu.clear();

    var fileMenu = menu.addMenu("File");
    fileMenu.addAction("Quit").triggered.connect(this.onQuitMenuCommand);
    fileMenu.addSeparator();    
}

UserInterface.prototype.onQuitMenuCommand = function() {
    console.LogInfo(">> QUIT event");
    framework.Exit();    
}
