// Traffic Module (c) Matti Kuonanoja 2014 
//
// Controls streeting angle and calculates vehicle position in update cycles.

engine.IncludeFile("TrafficModule/MathTools.js")

var VEHICLE_LENGTH = 2.5; 

var MIN_VEHICLE_SPEED = 0;
var MAX_VEHICLE_SPEED = 20;

var MIN_STEERING_ANGLE = -35;
var MAX_STEERING_ANGLE = 35;
var MAX_STREERING_ANGLE_SPEED = 70;

var MAX_ACCELERATION = 4.0;
var MAX_BRAKE_ACCELERATION = 15.0;

function VehiclePhysics() {
    this.currentPosition = undefined;
    this.currentDirection = undefined;
    this.currentSpeed = undefined;
    
    this.targetSpeed = undefined;
    this.targetDirection = undefined;
    this.targetStreeringAngle = 0;
    
    this.vehicleLength = undefined
    this.minStreeringAngle = undefined;
    this.maxStreeringAngle = undefined;
    this.maxSpeed = undefined;
    this.minSpeed = undefined;
    this.maxAcceleration = undefined;
    this.maxBrakeAcceleration = undefined;
    this.maxStreeringAngleSpeed = undefined;
    
    this.currentSteeringAngle = 0;
    
    this.vehicleLength = VEHICLE_LENGTH;
    this.maxSpeed = MAX_VEHICLE_SPEED;
    this.minSpeed = MIN_VEHICLE_SPEED;
    this.maxAcceleration = MAX_ACCELERATION;
    this.maxBrakeAcceleration = MAX_BRAKE_ACCELERATION;
    this.maxStreeringAngleSpeed = MAX_STREERING_ANGLE_SPEED;
    this.maxStreeringAngle = MAX_STEERING_ANGLE;
    this.minStreeringAngle = MIN_STEERING_ANGLE;
}

VehiclePhysics.prototype.setInitialState = function(position, direction, speed) {
    this.currentPosition = position;
    this.currentDirection = direction;
    this.currentSpeed = speed;
    
    this.targetSpeed = speed;
    this.targetDirection = direction;
}

VehiclePhysics.prototype.setTargetDirection = function(direction) {
    this.targetDirection = direction;
}

VehiclePhysics.prototype.setTargetSpeed = function(speed) {
    this.targetSpeed = speed;
}

VehiclePhysics.prototype.getPosition = function() {
    return this.currentPosition;
}

VehiclePhysics.prototype.getDirection = function() {
    return this.currentDirection;
}

VehiclePhysics.prototype.getSpeed = function() {
    return this.currentSpeed;
}

VehiclePhysics.prototype.update = function(deltaTime) {
    profiler.BeginBlock("VehiclePhysics.prototype.update");
    this.updateTargetSteeringAngle();
    this.updateStreeringAngle(deltaTime);
    this.updateSpeed(deltaTime);
    this.updatePositionAndDirection(deltaTime);
    profiler.EndBlock("VehiclePhysics.prototype.update");
}

VehiclePhysics.prototype.updateTargetSteeringAngle = function() {
    profiler.BeginBlock("updateTargetSteeringAngle");
    this.targetStreeringAngle = this.neededDirectionChange();
    
    if (this.targetStreeringAngle < this.minStreeringAngle) {
        this.targetStreeringAngle = this.minStreeringAngle
    }
    if (this.targetStreeringAngle > this.maxStreeringAngle) {
        this.targetStreeringAngle = this.maxStreeringAngle
    }
    profiler.EndBlock("updateTargetSteeringAngle");
}

VehiclePhysics.prototype.neededDirectionChange = function() {
    var directionChange = this.targetDirection - this.currentDirection;
    if (directionChange < -180) {
        directionChange += 360;
    }
    if (directionChange > 180) {
        directionChange -= 360;
    }
    return directionChange;
}

VehiclePhysics.prototype.updateStreeringAngle = function(deltaTime) {
    profiler.BeginBlock("updateStreeringAngle");
    var deltaSteeringAngle = deltaTime * this.maxStreeringAngleSpeed;
    
    if (this.currentSteeringAngle > this.targetStreeringAngle) {
        this.currentSteeringAngle -= deltaSteeringAngle;
        if (this.currentSteeringAngle < this.targetStreeringAngle) {
            this.currentSteeringAngle = this.targetStreeringAngle;
        }
    } else {
        this.currentSteeringAngle += deltaSteeringAngle;
        if (this.currentSteeringAngle > this.targetStreeringAngle) {
            this.currentSteeringAngle = this.targetStreeringAngle;
        }
    }
    profiler.EndBlock("updateStreeringAngle");
}

VehiclePhysics.prototype.updateSpeed = function(deltaTime) {
    profiler.BeginBlock("updateSpeed");
    if (!this.currentSpeed === undefined || this.targetSpeed === undefined) {
        console.LogWarning(">> VehiclePhysics: Cannot update speed");
        console.LogInfo("  this.currentSpeed="+this.currentSpeed);
        console.LogInfo("  this.targetSpeed="+this.targetSpeed);
        profiler.EndBlock("updateSpeed");
        return;
    }
    var wantedSpeedChange = this.targetSpeed - this.currentSpeed;
    
    /*if( Math.abs(wantedSpeedChange) < 0.0001) {
        wantedSpeedChange = 0;
    }
    */

    if (wantedSpeedChange > 0) {
        var absoluteDeltaSpeed = Math.min(Math.abs(wantedSpeedChange), deltaTime * this.maxAcceleration);
        this.currentSpeed += absoluteDeltaSpeed;
        if (this.currentSpeed > this.maxSpeed) {
            this.currentSpeed = this.maxSpeed;
        }
    }
    if (wantedSpeedChange < 0) {
        var absoluteDeltaSpeed = Math.min(Math.abs(wantedSpeedChange), deltaTime * this.maxBrakeAcceleration);
        this.currentSpeed -= absoluteDeltaSpeed;
        if (this.currentSpeed < this.minSpeed) {
            this.currentSpeed = this.minSpeed;
        }
    }
    profiler.EndBlock("updateSpeed");
}

VehiclePhysics.prototype.updatePositionAndDirection = function(deltaTime) {
    profiler.BeginBlock("updatePositionAndDirection");
    if (this.currentSpeed == 0) {
        profiler.EndBlock("updatePositionAndDirection");
        return;
    }

    var deltaLength = deltaTime * this.currentSpeed;
    var frontTireDirection = this.currentDirection + this.currentSteeringAngle;
    var backTireDirection = this.currentDirection;
    
    var frontTirePosition = getRelativePosition(this.currentPosition, this.currentDirection, this.vehicleLength*0.5);
    var backTirePosition = getRelativePosition(this.currentPosition, this.currentDirection+180, this.vehicleLength*0.5);
    
    frontTirePosition = getRelativePosition(frontTirePosition, frontTireDirection, deltaLength);
    backTirePosition = getRelativePosition(backTirePosition, backTireDirection, deltaLength); // not exact correct but close enough...
    
    var posX = (frontTirePosition.x + backTirePosition.x) / 2;
    var posY = (frontTirePosition.y + backTirePosition.y) / 2;
    
    this.currentPosition = {x: posX, y: posY}; 
    this.currentDirection = radiansToDegrees(directionTo(backTirePosition.x, backTirePosition.y, frontTirePosition.x, frontTirePosition.y));
    profiler.EndBlock("updatePositionAndDirection");
}
