// (c) Matti Kuonanoja 2014 

GLOBAL_WORLD_CAMERA_ENTITY_OBJECT_NAME = "WorldCamera";

var CAMERA_REQUEST_TAG = "followVehicle";
var CAMERA_HORIZONTAL_DISTANCE_FROM_VEHICLE = 12;
var CAMERA_VERTICAL_DISTANCE_FROM_VEHICLE = 4.0;

function ViewManager(scene) {
    this.scene = scene;
    
    this.keyboardConnected = false;
    this.worldCameraEntity = undefined;
    this.selectedEntityObjectId = undefined;
    
    console.LogInfo(">> ViewManager: Initialized");
}

ViewManager.prototype.update = function(deltaTime) {
    profiler.BeginBlock("ViewManager update");
    if (!this.scene) {
        console.LogWarning(">> ViewManager: scene is unknown");
        profiler.EndBlock("ViewManager update");
        return;
    }
    if (!this.worldCameraEntity) {
        this.worldCameraEntity = this.scene.EntityByName(GLOBAL_WORLD_CAMERA_ENTITY_OBJECT_NAME);
    }
    if (this.worldCameraEntity) {
        if (!this.keyboardConnected) {
            this.__connectToKeyboard(this.worldCameraEntity);
            this.keyboardConnected = true;
        }
        
        var vehicleId = this.worldCameraEntity.dynamiccomponent.GetAttribute(CAMERA_REQUEST_TAG);
        if (vehicleId) {
            var vehicleEntityObject = this.scene.EntityById(vehicleId);
            if (vehicleEntityObject) {
                this.selectedEntityObjectId = vehicleEntityObject.id;
                var vehicleTransform = vehicleEntityObject.placeable.transform;

                vehicleTransform.rot.y += 180;

                vehicleTransform.pos.y += CAMERA_VERTICAL_DISTANCE_FROM_VEHICLE;
                vehicleTransform.pos.x += CAMERA_HORIZONTAL_DISTANCE_FROM_VEHICLE*Math.sin(degreesToRadians(vehicleTransform.rot.y)) ;
                vehicleTransform.pos.z += CAMERA_HORIZONTAL_DISTANCE_FROM_VEHICLE*Math.cos(degreesToRadians(vehicleTransform.rot.y)) ;
                
                this.worldCameraEntity.placeable.transform = vehicleTransform;
            } else {
                console.LogWarning(">> ViewManager: Cannot find vehicle entity object!");                
            }
        } else {
            this.selectedEntityObjectId = undefined;
        }
    } else {
        console.LogWarning(">> ViewManager: Cannot find world camera entity object!");    
    }
    profiler.EndBlock("ViewManager update");
}

ViewManager.prototype.__connectToKeyboard = function(worldCameraEntity) {
    var context = worldCameraEntity.inputmapper.GetInputContext();
    
    var self = this;
    context.KeyPressed.connect(function(keyEvent){
            self.disableFollowMode();
        }
    );
    
    context.MouseScroll.connect(function(mouseEvent){
            var mouseWheelMovement = mouseEvent.relativeZ;
            if (mouseWheelMovement > 0) {
                self.moveCameraForward(50);
            }
            if (mouseWheelMovement < 0) {
                self.moveCameraForward(-50);
            }
            self.disableFollowMode();
        }
    );    
    
    worldCameraEntity.inputmapper.RegisterMapping("5", "setFromTopView()", 3);
    worldCameraEntity.inputmapper.RegisterMapping("8", "setFromNorthView()", 3);
    worldCameraEntity.inputmapper.RegisterMapping("6", "setFromEastView()", 3);
    worldCameraEntity.inputmapper.RegisterMapping("2", "setFromSouthView()", 3);
    worldCameraEntity.inputmapper.RegisterMapping("4", "setFromWestView()", 3);
    worldCameraEntity.inputmapper.RegisterMapping("+", "increaseCameraAltitude()", 3);
    worldCameraEntity.inputmapper.RegisterMapping("-", "decreaseCameraAltitude()", 3);
    worldCameraEntity.Action("setFromTopView").Triggered.connect(this, this.setFromTopView);
    worldCameraEntity.Action("setFromNorthView").Triggered.connect(this, this.setFromNorthView);
    worldCameraEntity.Action("setFromEastView").Triggered.connect(this, this.setFromEastView);
    worldCameraEntity.Action("setFromSouthView").Triggered.connect(this, this.setFromSouthView);
    worldCameraEntity.Action("setFromWestView").Triggered.connect(this, this.setFromWestView);
    worldCameraEntity.Action("increaseCameraAltitude").Triggered.connect(this, this.increaseCameraAltitude);
    worldCameraEntity.Action("decreaseCameraAltitude").Triggered.connect(this, this.decreaseCameraAltitude);
}

ViewManager.prototype.disableFollowMode = function(keyEvent) {
    var worldCameraEntity = this.scene.EntityByName(GLOBAL_WORLD_CAMERA_ENTITY_OBJECT_NAME);
    if (worldCameraEntity) {
        worldCameraEntity.dynamiccomponent.SetAttribute(CAMERA_REQUEST_TAG, "");
    }
}

ViewManager.prototype.increaseCameraAltitude = function(keyEvent) {
    this.disableFollowMode();
    if (!this.scene) {
        return;
    }
    var worldCameraEntity = this.scene.EntityByName(GLOBAL_WORLD_CAMERA_ENTITY_OBJECT_NAME);
    var transform = worldCameraEntity.placeable.transform;

    transform.pos.y += 50;

    worldCameraEntity.placeable.transform = transform;
}

ViewManager.prototype.moveCameraForward = function(distance) {
    var worldCameraEntity = this.scene.EntityByName(GLOBAL_WORLD_CAMERA_ENTITY_OBJECT_NAME);
    var movement = worldCameraEntity.placeable.Orientation().Mul(new float3(0,0,-distance));
    worldCameraEntity.placeable.SetPosition(worldCameraEntity.placeable.Position().Add(movement));
}

ViewManager.prototype.decreaseCameraAltitude = function() {
    this.disableFollowMode();
    if (!this.scene) {
        return;
    }
    var worldCameraEntity = this.scene.EntityByName(GLOBAL_WORLD_CAMERA_ENTITY_OBJECT_NAME);
    var transform = worldCameraEntity.placeable.transform;

    transform.pos.y -= 50;

    worldCameraEntity.placeable.transform = transform;
}

ViewManager.prototype.setFromTopView = function() {
    this.disableFollowMode();
    if (!this.scene) {
        return;
    }
    var worldCameraEntity = this.scene.EntityByName("WorldCamera");
    var transform = worldCameraEntity.placeable.transform;

    transform.pos.x = 50;
    transform.pos.y = 1000;
    transform.pos.z = 50;

    transform.rot.x = -90;
    transform.rot.y = 237.5+45;
    transform.rot.z = 0

    worldCameraEntity.placeable.transform = transform;
}

ViewManager.prototype.setFromNorthView = function() {
    this.disableFollowMode();
    if (!this.scene) {
        return;
    }
    var worldCameraEntity = this.scene.EntityByName("WorldCamera");
    var transform = worldCameraEntity.placeable.transform;

    transform.pos.x = 947.16;
    transform.pos.y = 692.48;
    transform.pos.z = -92.31;

    transform.rot.x = -38.70;
    transform.rot.y = 101.90;
    transform.rot.z = 0.00;

    worldCameraEntity.placeable.transform = transform;
}

ViewManager.prototype.setFromEastView = function() {
    this.disableFollowMode();
    if (!this.scene) {
        return;
    }
    var worldCameraEntity = this.scene.EntityByName("WorldCamera");
    var transform = worldCameraEntity.placeable.transform;

    transform.pos.x = 181.75;
    transform.pos.y = 647.75;
    transform.pos.z = 940.58;

    transform.rot.x = -39.00;
    transform.rot.y = 4.70;
    transform.rot.z = 0;
    
    worldCameraEntity.placeable.transform = transform;
}

ViewManager.prototype.setFromSouthView = function() {
    this.disableFollowMode();
    if (!this.scene) {
        return;
    }
    var worldCameraEntity = this.scene.EntityByName("WorldCamera");
    var transform = worldCameraEntity.placeable.transform;

    transform.pos.x = -721.90;
    transform.pos.y = 597.73;
    transform.pos.z = 446.98;

    transform.rot.x = -36.90;
    transform.rot.y = -76.00;
    transform.rot.z = 0.00;

    worldCameraEntity.placeable.transform = transform;
}

ViewManager.prototype.setFromWestView = function() {
    this.disableFollowMode();
    if (!this.scene) {
        return;
    }
    var worldCameraEntity = this.scene.EntityByName("WorldCamera");
    var transform = worldCameraEntity.placeable.transform;

    transform.pos.x = -56.20;
    transform.pos.y = 568.02;
    transform.pos.z = -870.62;

    transform.rot.x = -29.70;
    transform.rot.y = 185.00;
    transform.rot.z = 0.00;

    worldCameraEntity.placeable.transform = transform;
}
