// (c) Matti Kuonanoja 2014 

engine.IncludeFile("TrafficModule/Route.js");
engine.IncludeFile("TrafficModule/traffic/virtual_traffic.js");

// All Routes are sorted by start time begining from the first
function TrafficDatabase() {
    this.routes = [];
    this.__firstTimestamp = undefined;
    this.__lastTimestamp = undefined;
    this.__totalRouteLength = undefined;
    this.__load();
    this.__sortByTime();
    console.LogInfo(">> TrafficDatabase: initialized");
}

TrafficDatabase.prototype.__load = function() {
    this.__totalRouteLength = 0;
    for (var i = 0; i < dataFileCount; i++) {
        var trafficDataObject = eval("trafficData"+i);
        for (var j = 0; j < trafficDataObject.traffic.length; j++) {
            var routeData = trafficDataObject.traffic[j];
            var route = new Route(routeData.id, routeData.waypoints);
            route.type = routeData.type;
            var routeStartTimestamp = route.getFirstTimestamp();
            var routeLastTimestamp = route.getLastTimestamp();
            if (!this.__firstTimestamp || routeStartTimestamp < this.__firstTimestamp) {
                this.__firstTimestamp = routeStartTimestamp;
            }
            if (!this.__lastTimestamp || routeLastTimestamp > this.__lastTimestamp) {
                this.__lastTimestamp = routeLastTimestamp;
            }
            this.__totalRouteLength += route.getLength();
            this.routes.push(route);
        }
        delete trafficDataObject.traffic;
    }
    
    console.LogInfo(">> TrafficDatabase: "+this.routes.length+" routes loaded to memory");    
}

TrafficDatabase.prototype.getFirstTimestamp = function() {
    return this.__firstTimestamp;
}

TrafficDatabase.prototype.getLastTimestamp = function() {
    return this.__lastTimestamp;
}

TrafficDatabase.prototype.getRouteCount = function() {
    return this.routes.length;
}

TrafficDatabase.prototype.getTotalRouteLength = function() {
    return this.__totalRouteLength;
}

TrafficDatabase.prototype.getAllRoutes = function() {
    return this.routes;
}

TrafficDatabase.prototype.getRouteByIndex = function(index) {
    return this.routes[index];
}

TrafficDatabase.prototype.getTotalTime = function() {
    var totalTime = this.getLastTimestamp() - this.getFirstTimestamp();
    return totalTime;
}

TrafficDatabase.prototype.__sortByTime = function() {
    this.routes.sort(function(a,b) {
        if (a.startTime == b.startTime) {
            return a.endTime - b.endTime;
        } else {
            return a.startTime - b.startTime;
        }
    });
    console.LogInfo(">> TrafficDatabase: routes sorted by start time");
}
  