// (c) Matti Kuonanoja 2014 
//
// Encapsulates all vehicle functionality for one vehicle instance.

engine.IncludeFile("TrafficModule/MathTools.js")
engine.IncludeFile("TrafficModule/VehicleEntity.js")
engine.IncludeFile("TrafficModule/VehiclePhysics.js")
engine.IncludeFile("TrafficModule/RouteNavigator.js")

// speed controlling
var MAX_CURVE_SPEED = 6.0;
var MAX_PAUSE_APPROACHING_SPEED = 5.0;

// speed optimization
var MAX_UPDATE_DELTA_TIME = 1.0;
var TARGET_SPEED_UPDATE_INTERVAL = 0.5

var Vehicle = function(scene, route, startTimestamp) {
    this.scene = scene;
    this.route = route;
    this.simulationTime = startTimestamp;
    
    this.isRouteCompleted = false; // p
    this.routeNavigator = undefined;
    this.vehicleEntity = undefined;
    this.vehiclePhysics = undefined;
    this.targetSpeed = undefined;
    this.targetDirection = undefined;
    
    this.__initRouteNavigator(route, startTimestamp);
    this.__initVehiclePhysics(startTimestamp);
    this.__initVehicleEntity(scene);
    
    this.vehicleEntity.setHeadLights(false);
}

Vehicle.prototype.__initRouteNavigator = function(route, simulationTime) {
    this.routeNavigator = new RouteNavigator(route);    
    this.routeNavigator.setInitPauseTime(simulationTime);
}

Vehicle.prototype.__initVehicleEntity = function(scene) {
    var vehicleType = "blue-bora";
    if (this.route.type == "gpx") {
        vehicleType = "gray-bora";
    }
    if (this.route.type == "VehicleTrackingSystem") {
        vehicleType = "yellow-bora";
    }
    
    this.vehicleEntity = new VehicleEntity(scene, vehicleType);
    this.vehicleEntity.addToScene();
    this.__updateVehicleEntity();
}

Vehicle.prototype.__initVehiclePhysics = function(simulationTime) {
    var initialPosition = this.routeNavigator.getInitialPosition(simulationTime);
    var initialSpeed = this.routeNavigator.getInitialSpeed(simulationTime);
    var initialDirection = this.routeNavigator.getInitialDirection(simulationTime);
    
    this.vehiclePhysics = new VehiclePhysics();
    this.vehiclePhysics.setInitialState(initialPosition, initialDirection, initialSpeed);
}

Vehicle.prototype.isPaused = function() {
    return this.routeNavigator.isConsumingPauseTime();
}

Vehicle.prototype.getSpeed = function() {
    return this.vehiclePhysics.getSpeed();
}

Vehicle.prototype.getRouteLeft = function() {
    return this.routeNavigator.getRouteLeft();
}

Vehicle.prototype.getWaypointsLeftCount = function() {
    return this.routeNavigator.getWaypointsLeftCount();
}

Vehicle.prototype.update = function(deltaTime) {
    if (deltaTime === undefined) {
        return;
    }
    profiler.BeginBlock("Vehicle update");
    var updateCount = Math.max(1, Math.ceil(deltaTime / MAX_UPDATE_DELTA_TIME));
    var deltaTimePerUpdate = deltaTime / updateCount;
    
    for (var i = 0; i < updateCount; i++) {
        this.simulationTime += deltaTimePerUpdate;
        this.__updateRouteNavigator(deltaTimePerUpdate);
        this.__updateTargetSpeed(deltaTimePerUpdate);
        this.__updateTargetDirection();
        this.__updateVehiclePhysics(deltaTimePerUpdate);
        this.__updateVehicleEntity(deltaTimePerUpdate);  
    }
    profiler.EndBlock("Vehicle update");    
}

Vehicle.prototype.__updateTargetSpeed = function(deltaTime) {
    profiler.BeginBlock("updateTargetSpeed");
    if (this.targetSpeed === undefined || this.timeForLastSpeedUpdate === undefined || this.timeForLastSpeedUpdate > TARGET_SPEED_UPDATE_INTERVAL) {
        var currentPosition = this.vehiclePhysics.getPosition();
        var targetPosition = this.routeNavigator.getTargetPosition();
        this.targetSpeed = this.__getOptimalSpeed(currentPosition, targetPosition);
        this.timeForLastSpeedUpdate = 0;
    } else {
        this.timeForLastSpeedUpdate += deltaTime;
    }
    profiler.EndBlock("updateTargetSpeed");
}

Vehicle.prototype.__updateTargetDirection = function() {
    profiler.BeginBlock("updateTargetDirection");
    var currentPosition = this.vehiclePhysics.getPosition();
    var targetPosition = this.routeNavigator.getTargetPosition();
    this.targetDirection = this.__getOptimalDirection(currentPosition, targetPosition);
    profiler.EndBlock("updateTargetDirection");
}

Vehicle.prototype.__updateVehiclePhysics = function(deltaTime) {
    profiler.BeginBlock("updateVehiclePhysics");
    this.vehiclePhysics.setTargetDirection(this.targetDirection);
    this.vehiclePhysics.setTargetSpeed(this.targetSpeed);
    this.vehiclePhysics.update(deltaTime);    
    profiler.EndBlock("updateVehiclePhysics");
}

Vehicle.prototype.__updateRouteNavigator = function(deltaTime) {
    profiler.BeginBlock("updateRouteNavigator");
    var currentPosition = this.vehiclePhysics.getPosition();
    this.routeNavigator.setCurrentPosition(currentPosition);
    this.routeNavigator.setCurrentTime(this.simulationTime);
    this.routeNavigator.update(deltaTime);
    if (this.routeNavigator.isRouteCompleted()) {
        this.isRouteCompleted = true;
    }
    profiler.EndBlock("updateRouteNavigator");
}

Vehicle.prototype.__updateVehicleEntity = function(deltaTime) {
    profiler.BeginBlock("updateVehicleEntity");
    var currentPosition = this.vehiclePhysics.getPosition();
    var currentDirection = this.vehiclePhysics.getDirection();
    var currentSpeed = this.vehiclePhysics.getSpeed();
    
    profiler.BeginBlock("updateTerrainLevel");
    this.vehicleEntity.updateTerrainLevel(deltaTime);
    profiler.EndBlock("updateTerrainLevel");
    
    profiler.BeginBlock("setPositionAndDirection");
    this.vehicleEntity.setPositionAndDirection(currentPosition, currentDirection);
    profiler.EndBlock("setPositionAndDirection");
    
    if (GLOBAL_EXHAUST_ENABLED ) {
        this.vehicleEntity.setExhaustEffect(true);
    } else {
        this.vehicleEntity.setExhaustEffect(false);
    }    
    
    if (GLOBAL_HEAD_LIGHTS_ENABLED) {
        this.vehicleEntity.setHeadLights(true);
    } else {
        this.vehicleEntity.setHeadLights(false);    
    }
    
    if (deltaTime > 0 && GLOBAL_AUDIO_ENABLED) {
        this.vehicleEntity.enableAudio(true);
    } else {
        this.vehicleEntity.enableAudio(false);
    }
    
    if (GLOBAL_TRAIL_EFFECT_ENABLED) {
        if (this.vehiclePhysics.getSpeed() == 0) {
            this.vehicleEntity.setTrailEffect(true, true);
        } else {
            this.vehicleEntity.setTrailEffect(true, false);
        }
    } else {
        this.vehicleEntity.setTrailEffect(false);    
    }
    
    if (GLOBAL_GLOW_EFFECT_ENABLED) {
        this.vehicleEntity.setGlowEffect(true);
    } else {
        this.vehicleEntity.setGlowEffect(false);    
    }
    
    profiler.EndBlock("updateVehicleEntity");
}

Vehicle.prototype.__getOptimalSpeed = function(currentPosition, targetPosition) {
    if (this.routeNavigator.isConsumingPauseTime() || this.routeNavigator.isRouteCompleted()) {
        return 0;
    }    
    
    if (!targetPosition) {
        return 0;
    }    

    var speed = undefined;
    
    var timeToReachTarget = this.routeNavigator.getTargetTime() - this.simulationTime;
    var targetDistance = distanceTo(currentPosition.x, currentPosition.y, targetPosition.x, targetPosition.y);
    
    if (timeToReachTarget > 0 ) {
        speed = targetDistance / timeToReachTarget;
    } else {
        speed = this.vehiclePhysics.maxSpeed;
    }
    if (this.routeNavigator.isCurveComing(currentPosition)) {
        speed = Math.min(speed, MAX_CURVE_SPEED);
    } 
    if (this.routeNavigator.isPauseComing(currentPosition)) {
        speed = Math.min(speed, MAX_PAUSE_APPROACHING_SPEED);
    } 
    
    return speed;
}

Vehicle.prototype.__getOptimalDirection = function(currentPosition, targetPosition) {
    if (!targetPosition) {
        return undefined;
    }   
    var directionInRadians = directionTo(currentPosition.x, currentPosition.y, targetPosition.x, targetPosition.y);
    return radiansToDegrees(directionInRadians);
}

Vehicle.prototype.removeEntityFromScene = function() {
    this.vehicleEntity.removeFromScene();
}