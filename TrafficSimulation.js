// (c) Matti Kuonanoja 2014 
//
// Controls simulation update process and create new vehicles for new routes
// and removes old vehicles when routes are finished.

engine.IncludeFile("TrafficModule/TrafficDatabase.js");
engine.IncludeFile("TrafficModule/TrafficManager.js");
engine.IncludeFile("TrafficModule/VehicleManager.js");

var MIN_PLAYBACK_SPEED = 0.1;
var MAX_PLAYBACK_SPEED = 10.0;

GLOBAL_SIMULATION_SPEED = 1.0

function TrafficSimulation(scene) {
    this.scene = scene;
    
    this.simulationTime = undefined;
    this.running = false;
    this.playbackSpeed = 1.0;
    
    this.trafficDatabase = new TrafficDatabase();
    this.trafficManager = new TrafficManager(this.trafficDatabase);
    this.vehicleManager = new VehicleManager(this.scene);
    
    var startTime = this.trafficDatabase.getFirstTimestamp();     
    this.setSimulationTime(startTime);

    console.LogInfo(">> TrafficSimulation: Initialized");
}

TrafficSimulation.prototype.start = function() {
    if (!this.running) {
        this.running = true;
        console.LogInfo(">> TrafficSimulation: Started");
    }
}

TrafficSimulation.prototype.stop = function() {
    if (this.running) {
        this.running = false;
        console.LogInfo(">> TrafficSimulation: Stopped");
    }
}

TrafficSimulation.prototype.toggleRunningState = function() {
    if (this.running) {
        this.stop();
    } else {
        this.start();
    }
}

TrafficSimulation.prototype.setPlaybackSpeed = function(speed) {
    if (speed >= MIN_PLAYBACK_SPEED && speed <= MAX_PLAYBACK_SPEED) {
        this.playbackSpeed = speed;
        GLOBAL_SIMULATION_SPEED = speed;
        console.LogInfo(">> TrafficSimulation: Set playback speed to "+this.playbackSpeed);
    } else {
        console.LogWarning(">> TrafficSimulation: Trying to set illegal playback speed: "+speed);
    }
}

TrafficSimulation.prototype.setSimulationTime = function(timestamp) {
    var wasRunning = this.running;
    this.stop();
    this.vehicleManager.removeAllVehicles();
    
    this.simulationTime = timestamp;
    var timeText = new Date(timestamp*1000);
    console.LogInfo(">> TrafficSimulation: Time set to : "+this.simulationTime+" ("+timeText.toString()+")");
    this.createInitialVehicles();
    
    if (wasRunning) {
        this.start();
    }
}

TrafficSimulation.prototype.getSimulationStartTime = function() {
    return this.trafficDatabase.firstTimestamp;
}

TrafficSimulation.prototype.getSimulationEndTime = function() {
    return this.trafficDatabase.lastTimestamp;
}

TrafficSimulation.prototype.isRunning = function() {
    return this.running;
}

TrafficSimulation.prototype.getSimulationTime = function() {
    return this.simulationTime;
}

TrafficSimulation.prototype.getStartTime = function() {
    return this.trafficDatabase.getFirstTimestamp();
}

TrafficSimulation.prototype.getEndTime = function() {
    return this.trafficDatabase.getLastTimestamp();
}

TrafficSimulation.prototype.getAverageSpeed = function() {
    return this.vehicleManager.getAverageSpeed();
}

TrafficSimulation.prototype.getVehicleCount = function() {
    return this.vehicleManager.getVehicleCount();
}

TrafficSimulation.prototype.getAverageRouteLength = function() {
    return this.vehicleManager.getAverageRouteLength();
}

TrafficSimulation.prototype.update = function(deltaTime) {
    profiler.BeginBlock("TrafficSimulation update");
    var simulationDeltaTime = this.playbackSpeed * deltaTime;
    
    if (!this.simulationTime) {
        console.LogWarning(">> TrafficSimulation: ERROR, cannot update simulation time, time not set");    
        profiler.EndBlock("TrafficSimulation update");
        return;
    }
    
    if (this.running) {
        this.simulationTime += simulationDeltaTime;
        
        if (this.simulationTime > this.getEndTime()) {
            console.LogInfo(">> TrafficSimulation: Simulation ended.");
            this.stop();
            this.simulationTime = this.getEndTime();
        }
        
        this.vehicleManager.removeFinishedVehicles(this.simulationTime);
        this.createNewVehicles(deltaTime);
        
    } else {
        simulationDeltaTime = 0;
    }
    
    this.vehicleManager.update(simulationDeltaTime);
    profiler.EndBlock("TrafficSimulation update");
}

TrafficSimulation.prototype.createNewVehicles = function(deltaTime) {
    profiler.BeginBlock("createNewVehicles");
    var newRoutes = this.trafficManager.getNewActiveRoutes(this.simulationTime);
    if (newRoutes.length > 0) {
        console.LogInfo(">> TrafficSimulation: Found "+newRoutes.length+" new active routes.");
        this.createVehiclesForRoutes(newRoutes);
    }
    profiler.EndBlock("createNewVehicles");
}

TrafficSimulation.prototype.createInitialVehicles = function() {
    this.trafficManager.moveToFirstUnfinishedRoute(this.simulationTime);
    var newRoutes = this.trafficManager.getNewActiveRoutes(this.simulationTime);
    if (newRoutes.length > 0) {
        console.LogInfo(">> TrafficSimulation: Found "+newRoutes.length+" initial active routes.");
        this.createVehiclesForRoutes(newRoutes);        
    } else {
        console.LogInfo(">> TrafficSimulation: Found 0 initial active routes.");        
    }
}

TrafficSimulation.prototype.createVehiclesForRoutes = function(routes) {
    for (var i = 0; i < routes.length; i++) {
        var route = routes[i];
        this.vehicleManager.createVehicle(route, this.simulationTime);    
    }
}
