// (c) Matti Kuonanoja 2014 

var locationToSimulationPosition = function(latitude, longitude) {
    
    var x = latitude;
    var y = longitude;
    
    var origoLatitude = 65.011801;
    var origoLongitude = 25.470171;

    var latitudeInMeters = 111493;
    var longitudeInMeters = 47151;
    var northDirection = 90;
    var eastDirection = 0;
    var deltaLatitude = latitude - origoLatitude;
    var deltaLongitude = longitude - origoLongitude;
    
    x = latitudeInMeters*deltaLatitude*Math.sin(degreesToRadians(northDirection)) + longitudeInMeters*deltaLongitude*Math.sin(degreesToRadians(eastDirection));
    y = latitudeInMeters*deltaLatitude*Math.cos(degreesToRadians(northDirection)) + longitudeInMeters*deltaLongitude*Math.cos(degreesToRadians(eastDirection));
    
    return {"x": x, "y": y};
}

var getDistance = function(locationA, locationB) {
    positionA = locationA.toSimulationPosition();
    positionB = locationB.toSimulationPosition();
    distanceTo(positionA.x, positionA.y, positionB.x, positionB.y)
    return Math.sqrt( (a-x)*(a-x)+(b-y)*(b-y));
}

// for simulation positions
var distanceTo = function(a, b, x, y) {
    return Math.sqrt( (a-x)*(a-x)+(b-y)*(b-y));
}

// todo: return degrees, not radians
var directionTo = function(x1, y1, x2, y2) {
    var deltaX = x2-x1;
    var deltaY = y2-y1;
    var dir = Math.atan2(deltaX, deltaY);

    return dir;
}

var radiansToDegrees = function(angle) {
    return angle*360/2/Math.PI;
}

var degreesToRadians = function(angle) {
    return angle*Math.PI*2/360;
}

var getDeltaDirection = function (direction1, direction2) {
    //return (direction1 - direction2 + 180) % 360 - 180;
    //return 180.0 - Math.abs(Math.abs(direction1 - direction2)-180);
    
    var delta;
    if (direction1 > direction2) {
        delta = direction1 - direction2;
        if (delta > 180) {
            delta = delta-360;
            //delta = 360-delta;
        }
        return -delta;
    } else {
        delta = direction2 - direction1;
        if (delta > 180) {
            delta = delta-360;
        }        
    }
    return delta;
}

var normalizeDegrees = function(angle) {
    var i = Math.floor(angle / 360);
    var angle2 = angle - i*360;
    if (angle2 < 0) {
        angle2 += 360;
    }
    if ( Math.round((angle+360) % 360) != Math.round((angle2+360) % 360)) {
        console.LogError("normalizeDegrees ERROR");
        console.LogError(""+angle+"->"+angle2);
    }
    return angle2;
 }
 
var getRelativePosition = function(position, direction, range) {
    var directionInRadians = degreesToRadians(direction);
    var deltaX = range * Math.sin(directionInRadians);
    var deltaY = range * Math.cos(directionInRadians);
    return {x: position.x + deltaX, y: position.y + deltaY};
 }
 