// (c) Matti Kuonanoja 2014 

engine.IncludeFile("TrafficModule/TrafficSimulation.js")
engine.IncludeFile("TrafficModule/UserInterface.js");
engine.IncludeFile("TrafficModule/SceneManager.js");
engine.IncludeFile("TrafficModule/ViewManager.js");

var START_TIME_ADJUST_IN_SECONDS = 0;
var UI_REFRESH_TIME_INTEVAL = 0.1;

function TrafficApplication(scene) {
    this.scene = scene;
    
    this.trafficSimulation = undefined;
    this.sceneManager = undefined;
    this.userInterface = undefined;
    this.viewManager = undefined;
    
    this.__initViewManager(scene);
    this.__initSceneManager(scene);
    this.__initTrafficSimulation(scene);
    this.__initUserInterface(this.trafficSimulation, this.sceneManager);
    
    this.__registerConsoleCommands();
    
    console.LogInfo(">> TrafficApplication: Initialized");
}

TrafficApplication.prototype.__initTrafficSimulation = function(scene) {
    this.trafficSimulation = new TrafficSimulation(scene);
    console.LogInfo(">> TrafficApplication: traffic simulation initialized");    
}

TrafficApplication.prototype.__initSceneManager = function(scene) {
    this.sceneManager = new SceneManager(scene);
    
    this.sceneManager.flatInfraEnabled = false;
    this.sceneManager.transparentInfraEnabled = false;
    this.sceneManager.bigVehiclesEnabled = false;
    this.sceneManager.exhaustEffectEnabled = false;
    this.sceneManager.showIntersectionsEnabled = false;
    this.sceneManager.nightModeEnabled = false;
    this.sceneManager.forceSettings();

    console.LogInfo(">> TrafficApplication: scene manager initialized");    
}

TrafficApplication.prototype.__initViewManager = function(scene) {
    this.viewManager = new ViewManager(scene);
    
    console.LogInfo(">> TrafficApplication: camera controller initialized");            
}

TrafficApplication.prototype.__initUserInterface = function(trafficSimulation, sceneManager) {
    this.userInterface = new UserInterface();
    
    this.userInterface.setSetTimeCallback(function(timestamp){trafficSimulation.setSimulationTime(timestamp);});
    this.userInterface.setToggleSimulationStateCallback(function(){trafficSimulation.toggleRunningState();});
    this.userInterface.setFlatInfraCallback(function(enabled){sceneManager.flatInfraEnabled = enabled;sceneManager.forceSettings();});
    this.userInterface.setTransparentInfraCallback(function(enabled){sceneManager.transparentInfraEnabled = enabled;sceneManager.forceSettings();});
    this.userInterface.setBigVehiclesCallback(function(enabled){sceneManager.bigVehiclesEnabled = enabled;sceneManager.forceSettings();});
    this.userInterface.setExhaustEffectCallback(function(enabled){sceneManager.exhaustEffectEnabled = enabled;sceneManager.trailEffectEnabled = false; sceneManager.forceSettings();});
    this.userInterface.setShowIntersectionsCallback(function(enabled){sceneManager.showIntersectionsEnabled = enabled;sceneManager.forceSettings();});
    this.userInterface.setSpeedCallback(function(speedFactor){trafficSimulation.setPlaybackSpeed(speedFactor);});
    this.userInterface.setNightModeCallback(function(enabled){sceneManager.nightModeEnabled = enabled;sceneManager.forceSettings();});
    this.userInterface.setTrailEffectCallback(function(enabled){sceneManager.trailEffectEnabled = enabled; sceneManager.exhaustEffectEnabled = false;sceneManager.forceSettings();});
    this.userInterface.setAudioCallback(function(enabled){sceneManager.audioEnabled = enabled;sceneManager.forceSettings();});
    this.userInterface.setGlowEffectCallback(function(enabled){sceneManager.glowEffectEnabled = enabled;sceneManager.forceSettings();});
    
    this.userInterface.setSimulationTimeFrame(trafficSimulation.getStartTime(), trafficSimulation.getEndTime());
    
    console.LogInfo(">> TrafficApplication: user interface manager initialized");            
}    

TrafficApplication.prototype.update = function(frameTime) {
    profiler.BeginBlock("TrafficApplication update");
    this.trafficSimulation.update(frameTime);
    this.__updateUserInterface(frameTime);
    this.viewManager.update(frameTime);
    profiler.EndBlock("TrafficApplication update");
}

TrafficApplication.prototype.__updateUserInterface = function(deltaTime) {
    profiler.BeginBlock("UserInterface update");
    if (!this.timeFromLastUiUpdate) {
        this.timeFromLastUiUpdate = 0;
    } else {
        this.timeFromLastUiUpdate += deltaTime;
        if (this.timeFromLastUiUpdate > UI_REFRESH_TIME_INTEVAL) {
            this.timeFromLastUiUpdate = 0;
        } else {
            profiler.EndBlock("UserInterface update");
            return;
        }
    }
    
    if (this.userInterface) {
        var simulationStateInfo = {
            totalSimulationTime: this.trafficSimulation.trafficDatabase.getTotalTime(),
            totalRouteCount: this.trafficSimulation.trafficDatabase.getRouteCount(),
            totalRouteLength: this.trafficSimulation.trafficDatabase.getTotalRouteLength(),
            currentVehicleCount: this.trafficSimulation.getVehicleCount(),
            currentAverageSpeed: this.trafficSimulation.getAverageSpeed(),
            currentAverageRouteLength: this.trafficSimulation.getAverageRouteLength()
        }
        this.userInterface.setSimulationStateInfo(simulationStateInfo);
        
        if (this.viewManager.selectedEntityObjectId) {
            var vehicle = this.trafficSimulation.vehicleManager.getVehicleByEntityObjectId(this.viewManager.selectedEntityObjectId);
            if (vehicle) {
                var vehicleStateInfo = {
                    id: vehicle.route.id,
                    type: vehicle.route.type,
                    routeLength: vehicle.route.getLength(),
                    waypointCount: vehicle.route.getWaypointCount(),
                    speed: vehicle.getSpeed(),
                    routeLeft: vehicle.getRouteLeft(),
                    waypointsLeft: vehicle.getWaypointsLeftCount()
                }   
                this.userInterface.setVehicleStateInfo(vehicleStateInfo);
            }
        } else {
            var vehicleStateInfo = this.vehicleStateInfo = {
                id: undefined,
                type: undefined,
                routeLength: undefined,
                waypointCount: undefined,
                speed: undefined,
                routeLeft: undefined,
                waypointsLeft: undefined
            }   
            this.userInterface.setVehicleStateInfo(vehicleStateInfo);
        }
        
        this.userInterface.updateSimulationTime(this.trafficSimulation.getSimulationTime());
        this.userInterface.updateSimulationState(this.trafficSimulation.isRunning(), this.trafficSimulation.playbackSpeed);
        
    } else {
        console.LogWarning(">> TrafficApplication: user interface manager not set!");
    }
    profiler.EndBlock("UserInterface update");
}

TrafficApplication.prototype.__registerConsoleCommands = function() {
   var t90Command = console.RegisterCommand("T90", ":)");
    t90Command.Invoked.connect(function() {
        GLOBAL_T90_MODE = true;
        console.LogInfo("Ready.");
    });
    

   var simulationtimeCommand = console.RegisterCommand("simulationtime", "Set the simulation time (unix timestamp format)");
   self = this;
   simulationtimeCommand.Invoked.connect(function(params) {
        if (params.length < 1) {
            console.LogInfo("Current simulation time: "+self.trafficSimulation.simulationTime);
            return;
        }
        var simulationTime = parseInt(params[0]);
        if (simulationTime < self.trafficSimulation.getSimulationStartTime() || simulationTime > self.trafficSimulation.getSimulationEndTime()) {
            console.LogInfo("Invalid simulation time: "+simulationTime);
            console.LogInfo("Simulation time scale: " +self.trafficSimulation.getSimulationStartTime()+  " - " + self.trafficSimulation.getSimulationEndTime());
            return;
        }
        self.trafficSimulation.setSimulationTime(simulationTime)
        console.LogInfo("Simulation time is set to: "+self.trafficSimulation.simulationTime);
    });
}
