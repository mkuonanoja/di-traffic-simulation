// Traffic Module (c) Matti Kuonanoja 2014 
// 
// Creates TrafficApplication instance and connects frame update signal
// to traffic application update function

console.LogInfo(">> TrafficModule: Started.");

engine.IncludeFile("TrafficModule/TrafficApplication.js");

var trafficApplication = undefined;

var updateSimulationApplication = function(frameTime) {
    if (trafficApplication) {
        trafficApplication.update(frameTime);
    }
}

function createSimulationApplication(scenename) {
    var scene = framework.Scene().GetScene(scenename);
    if (scene != null) {
        console.LogInfo(">> TrafficModule: New scene registered ["+scenename+"]");    
        
        if (trafficApplication) {
            console.LogError(">> TrafficModule: trying to create traffic application again!");
            return;
        }
        trafficApplication = new TrafficApplication(scene);
    }
}

if (!framework.IsHeadless()) 
{
    framework.Scene().SceneAdded.connect(createSimulationApplication);
    frame.Updated.connect(updateSimulationApplication);    
}
