// (c) Matti Kuonanoja 2014 
//
// Controls environment object and intersection reference point marker obects.
// Implements transparent and night effects.

engine.IncludeFile("TrafficModule/MathTools.js");
engine.IncludeFile("TrafficModule/streetmodel/intersection_ref_points.js");

GLOBAL_HEAD_LIGHTS_ENABLED = false;
GLOBAL_TRAIL_EFFECT_ENABLED = false;

GLOBAL_AUDIO_ENABLED = false;
GLOBAL_GLOW_EFFECT_ENABLED = false;

var WORLD_ENTITY_NAME = "CityModel";
var ENVIRONMENT_ENTITY_NAME = "Environment";
var FLAT_INFRA_PERSENTAGE = 0.10;
var NORMAL_SCALE = 0.0256;
var DAY_AMBIENT_COLOR = 200;
var NIGHT_AMBIENT_COLOR = 0;

function SceneManager(scene) {
    this.scene = scene;
    
    this.orginalWorldMeshMaterial = undefined;
    this.referencePointEntities = []
    
    this.flatInfraEnabled = false;
    this.transparentInfraEnabled = false;
    this.bigVehiclesEnabled = false;
    this.exhaustEffectEnabled = false;
    this.showIntersectionsEnabled = false;
    this.nightModeEnabled = false;
    this.audioEnabled = false;    
    this.glowEffectEnabled = false;

    var self = this;
    scene.EntityCreated.connect(function(entityObject, changeType) {
        if (entityObject.name == WORLD_ENTITY_NAME || entityObject.name == ENVIRONMENT_ENTITY_NAME ) {
            self.forceSettings();
        }
    });
    
    this.__setNightMode(this.nightModeEnabled);

    console.LogInfo(">> SceneManager: initialized");
}

// todo: refactore public interface
SceneManager.prototype.forceSettings = function() {
    this.__updateInfraScale();
    this.__updateInfraTexture();
    this.__showIntersections(this.showIntersectionsEnabled);
    if (this.bigVehiclesEnabled) {
        GLOBAL_VEHICLE_SCALE = 5.0;
    } else {
        GLOBAL_VEHICLE_SCALE = 1.0;
    }

    if (this.exhaustEffectEnabled) {
        GLOBAL_EXHAUST_ENABLED = true;
    } else {
        GLOBAL_EXHAUST_ENABLED = false;
    }
    
    if (this.trailEffectEnabled) {
        GLOBAL_TRAIL_EFFECT_ENABLED = true;
    } else {
        GLOBAL_TRAIL_EFFECT_ENABLED = false;
    }
    
    if (this.audioEnabled) {
        GLOBAL_AUDIO_ENABLED = true;
    } else {
        GLOBAL_AUDIO_ENABLED = false;
    }
    
    if (this.glowEffectEnabled) {
        GLOBAL_GLOW_EFFECT_ENABLED = true;
    } else {
        GLOBAL_GLOW_EFFECT_ENABLED = false;
    }

    this.__setNightMode(this.nightModeEnabled);
}

SceneManager.prototype.__setNightMode = function(enabled) {
    if (enabled) {
        var environmentEntityObject = this.scene.EntityByName("Environment");
        if (environmentEntityObject) {
            environmentEntityObject.skyX.time = 24.00;
            environmentEntityObject.skyX.ambientLightColor = new Color(0.00, 0.00, 0.00, 0.00);
            environmentEntityObject.skyX.moonlightDiffuseColor = new Color(0.125, 0.125, 0.125, 0.125);
            environmentEntityObject.skyX.moonlightSpecularColor = new Color(0.125, 0.125, 0.125, 0.125);
            environmentEntityObject.skyX.moonPhase = 50;
            environmentEntityObject.fog.color = new Color(0.12,0.12,0.24, 0.0);
        } else {
            console.LogWarning(">> SceneManager: Cannot set night mode, cannot find environment entity object");
        }
        GLOBAL_HEAD_LIGHTS_ENABLED = true;
    } else {
        var environmentEntityObject = this.scene.EntityByName("Environment");
        if (environmentEntityObject) {
            environmentEntityObject.skyX.time = 12.00;
            environmentEntityObject.skyX.ambientLightColor = new Color(0.364, 0.364000, 0.364000, 1);       
            environmentEntityObject.skyX.moonlightDiffuseColor = new Color(0.125,0.125,0.125, 0,125);
            environmentEntityObject.skyX.moonlightSpecularColor = new Color(0.125,0.125,0.125, 0,125);
            environmentEntityObject.fog.color = new Color(0.9,0.9,0.9, 0.9);
        } else {
            console.LogWarning(">> SceneManager: Cannot unset night mode, cannot find environment entity object");        
        }
        GLOBAL_HEAD_LIGHTS_ENABLED = false;
    }
    this.__updateInfraTexture();
}

SceneManager.prototype.__updateInfraScale = function() {
    var worldEntity = this.scene.EntityByName(WORLD_ENTITY_NAME);
    if (worldEntity) {
        var transform = worldEntity.placeable.transform;
        if (this.flatInfraEnabled) {
            transform.scale.y = NORMAL_SCALE * FLAT_INFRA_PERSENTAGE;
            transform.scale.x = NORMAL_SCALE;
            transform.scale.z = NORMAL_SCALE;
        } else {
            transform.scale.x = NORMAL_SCALE;
            transform.scale.y = NORMAL_SCALE;
            transform.scale.z = NORMAL_SCALE;
        }
        worldEntity.placeable.transform = transform;
    } 
}

SceneManager.prototype.__updateInfraTexture = function() {
    var worldEntity = this.scene.EntityByName(WORLD_ENTITY_NAME);
    if (worldEntity) {
        if (!this.orginalWorldMeshMaterial) {
            this.orginalWorldMeshMaterial = worldEntity.mesh.meshMaterial;
        }    
        var materialReferences = new AssetReferenceList();
        //var transform = worldEntity.placeable.transform;
        if (this.transparentInfraEnabled) {
            materialReferences = [];
            for(var i = 0; i < 126; i++) {
                if (this.nightModeEnabled) {
                    materialReferences.push("transparentinfra_night.material");
                } else {
                    materialReferences.push("transparentinfra_day.material");
                }
            }
            
            worldEntity.mesh.meshMaterial = materialReferences;
        } else {
            worldEntity.mesh.meshMaterial = this.orginalWorldMeshMaterial;
        }
    } 
}

SceneManager.prototype.__showIntersections = function(show) {
    if (show && this.referencePointEntities.length === 0) {
        this.__createIntersectionMarkers();
    }
    for (var i = 0; i < this.referencePointEntities.length; i++) {
        var entity = this.referencePointEntities[i];
        if (show) {
            entity.placeable.Show();
        } else {
           entity.placeable.Hide();
        }
    }
}

SceneManager.prototype.__createIntersectionMarkers = function() {
    this.referencePointEntities = [];
    var coordinates = referencepoints["coordinates"];
    for(var i = 0; i < coordinates.length / 2; i++) {
        var latitude = coordinates[i*2];
        var longitude = coordinates[i*2+1];
        var pos = locationToSimulationPosition(latitude, longitude);
        var x = pos["x"];
        var y = pos["y"];
        var entity = this.__createReferencePointEntity(x, y);
        entity.placeable.Hide();
        this.referencePointEntities.push(entity);
    }
    console.LogInfo(">> SceneManager: Created "+this.referencePointEntities.length+" intersection markers");
}

SceneManager.prototype.__createReferencePointEntity = function(x, y) {
    var HEIGHT = 100;
    var WIDTH = 0.5;
    var ENTITY_NAME = "Reference Point Marker";
    
    var entity = this.scene.CreateEntity(this.scene.NextFreeId(), ["EC_Name", "EC_Placeable", "EC_Mesh"]);
    entity.name = ENTITY_NAME;
    entity.SetTemporary(true);     
    
    // position
    var transform = entity.placeable.transform;
    transform.pos.x = x;
    transform.pos.y = HEIGHT*0.5 - 10;
    transform.pos.z = y;
    transform.scale.x = WIDTH;
    transform.scale.y = HEIGHT;
    transform.scale.z = WIDTH;
    
    entity.placeable.transform = transform;    
    
    // mesh and material
    entity.mesh.meshRef = "testcube.mesh";
    var materialReferences = new AssetReferenceList();
    materialReferences = ["testcube.material"];
    entity.mesh.meshMaterial = materialReferences;
    return entity;    
}
