// Traffic Module (c) Matti Kuonanoja 2014 
//
// - Adds/Removes Tundra entity for a single vehicle object
// - Updates entity properties (position, rotation)

GLOBAL_VEHICLE_SCALE = 1.0;
GLOBAL_EXHAUST_ENABLED = false;

var LIGHT_COMPONENT_ENABLED = false;

var MAX_TERRAIN_LEVEL_CHECKS_PER_SECOND = 500;
var MIN_TERRAIN_LEVEL_INTERVAL_IN_SECOONDS = 0.05;

var VEHICLE_ALTITUDE = 0.5;

var VEHICLE_MASS = 0;
var WORLD_RIGID_BODY_LAYER = 1;
var VEHICLE_RIGID_BODY_LAYER = 2;

var OUTER_SOUND_RADIUS_MAX = 100;
var INNER_SOUND_RADIUS = 20;
var SOUND_FILE_NAME = "23274__ljudman__helsinki2-25082006.ogg";

var HEAD_LIGHTS_BRIGHTNESS = 3.0;
var HEAD_LIGHTS_INNER_ANGLE = 20;
var HEAD_LIGHTS_OUTER_ANGLE = 40;
var HEAD_LIGHTS_RANGE = 100.0;

var SMOKE_PARTICLE_EFFECT = "smoke.particle";
var TRAIL_PARTICLE_EFFECT = "trail.particle";
var TRAIL_PARTICLE_EFFECT_X10 = "trailx10.particle";
var TRAIL_PARTICLE_EFFECT_X5 = "trailx5.particle";
var TRAIL_PARTICLE_EFFECT_X2 = "trailx2.particle";
var TRAIL_PARTICLE_EFFECT_STOPPED = "trail-stop.particle";
var TRAIL_PARTICLE_EFFECT_X05 = "trailx0.5.particle";

GLOBAL_T90_MODE = false;

// Manages Tundra entity which presents a vehicle object
var VehicleEntity = function(scene, vehicleType) {
    this.scene = scene;
    this.vehicleType = vehicleType;

    this.entityObject = undefined;
    this.timeFromLastUpdate = 0;
}

VehicleEntity.prototype.addToScene = function() {
    if (this.entityObject) {
        console.LogWarning(">> VehicleEntity: Entity object already in the scene!");
        this.removeFromScene();
    }
    this.entityObject = this.createVehicleEntity("vehicle");    
}

VehicleEntity.prototype.removeFromScene = function() {
    if (this.entityObject) {
        this.setWorldCameraToFollowMode();
        this.setTrailEffect(false);
        
        this.scene.RemoveEntity(this.entityObject.id);
        this.entityObject = undefined;
    } else {
        console.Warning(">> VehicleEntity: Cannot remove from scene, entity object doesn't exist.");
    }
}

VehicleEntity.prototype.setPositionAndDirection = function(position, direction, deltaTime) {
    if (this.transform === undefined) {
        this.transform = this.entityObject.placeable.transform;
    }

    if (this.terrainLevel === undefined) {
        console.LogWarning(">> VehicleEntity: Cannot find ground level.");
        this.updateTerrainLevel();
        this.terrainLevel = 0;
    }
  
    // position
    this.transform.pos.x = position.x;
    this.transform.pos.y = this.terrainLevel + VEHICLE_ALTITUDE;
    this.transform.pos.z = position.y;
    
    // direction
    this.transform.rot.x = 0;
    this.transform.rot.y = direction;
    this.transform.rot.z = 0;
     
    // scale
    this.transform.scale.x = GLOBAL_VEHICLE_SCALE;
    this.transform.scale.y = GLOBAL_VEHICLE_SCALE;
    this.transform.scale.z = GLOBAL_VEHICLE_SCALE;
    
    this.entityObject.placeable.transform = this.transform;
}

VehicleEntity.prototype.updateTerrainLevel = function(deltaTime) {
    if (deltaTime) {
        this.timeFromLastUpdate += deltaTime;
    }
   
    if (deltaTime == undefined || this.timeFromLastUpdate > this.__getTerrainLevelCheckInterval()) {
        this.terrainLevel = this.__getTerrainAltitude();
        this.timeFromLastUpdate = 0;
    } 
    
    if (GLOBAL_T90_MODE) {
        if (this.vehicleType != "T90") {
            this.enableT90Mode();
        }
    }
}

VehicleEntity.prototype.__getTerrainLevelCheckInterval = function() {
    var interval = Math.max(GLOBAL_TOTAL_VEHICLE_COUNT / MAX_TERRAIN_LEVEL_CHECKS_PER_SECOND, MIN_TERRAIN_LEVEL_INTERVAL_IN_SECOONDS);
    return interval;
}

VehicleEntity.prototype.__getTerrainAltitude = function() {
    if (!this.entityObject) {
        console.LogWarning(">> VehicleEntity: Cannot set altitude, entity object not set.");        
        return;
    }
    
    TERRAIN_VERTICAL_SEARCH_DISTANCE = 50;
 
    var scenePhysics = this.entityObject.ParentScene().physics;
    var vehicleOrigin = this.entityObject.placeable.Position()
    var searchPosition = vehicleOrigin;
    searchPosition.y += TERRAIN_VERTICAL_SEARCH_DISTANCE * 0.5;
    
    var downDirection = float3(0,-1,0);
    var upDirection = float3(0,1,0);
    
    var raycastResult = scenePhysics.Raycast(vehicleOrigin, downDirection, TERRAIN_VERTICAL_SEARCH_DISTANCE, WORLD_RIGID_BODY_LAYER, WORLD_RIGID_BODY_LAYER);
    if (raycastResult) {
        var groundLevel = raycastResult.pos.y;
        return groundLevel;
    } else {
        return 0;
        return undefined;
    }
}

VehicleEntity.prototype.setExhaustEffect = function(enabled) {
    if (this.exhaustEffectEnabled === undefined || enabled !== this.exhaustEffectEnabled) {
        this.exhaustEffectEnabled = enabled;        
        if (enabled) {
            this.entityObject.particlesystem.particleRef = SMOKE_PARTICLE_EFFECT;
            this.entityObject.particlesystem.enabled = true;
        } else {
            if (!this.trailEffectEnabled) {
                this.entityObject.particlesystem.enabled = false;    
            }
        }
    }
}

VehicleEntity.prototype.setTrailEffect = function(enabled, vehicleStopped) {
    if (enabled !== this.trailEffectEnabled || this.lastKnownSimulationSpeed !== GLOBAL_SIMULATION_SPEED) {
        this.trailEffectEnabled = enabled;        
        this.lastKnownSimulationSpeed = GLOBAL_SIMULATION_SPEED;
        if (enabled) {
            //TRAIL_PARTICLE_EFFECT_STOPPED
            this.entityObject.particlesystem.enabled = false;
            if (GLOBAL_SIMULATION_SPEED == 10) {
                this.entityObject.particlesystem.particleRef = TRAIL_PARTICLE_EFFECT_X10;
            } else if (GLOBAL_SIMULATION_SPEED == 5) {
                this.entityObject.particlesystem.particleRef = TRAIL_PARTICLE_EFFECT_X5;            
            } else if (GLOBAL_SIMULATION_SPEED == 2) {
                this.entityObject.particlesystem.particleRef = TRAIL_PARTICLE_EFFECT_X2;                        
            } else if (GLOBAL_SIMULATION_SPEED == 0.5) {
                this.entityObject.particlesystem.particleRef = TRAIL_PARTICLE_EFFECT_X05;                        
            } else {
                this.entityObject.particlesystem.particleRef = TRAIL_PARTICLE_EFFECT;            
            }
            this.entityObject.particlesystem.enabled = true;
        } else {
            if (!this.exhaustEffectEnabled) {        
                this.entityObject.particlesystem.enabled = false;    
            }
        }
    }
}

VehicleEntity.prototype.setHeadLights = function(enabled) {
    if (enabled != this.headLightstEffectEnabled) {
        this.headLightstEffectEnabled = enabled;    
        if (!this.entityObject.light) {
            return;
        }
        if (enabled) {
            this.entityObject.light.range = HEAD_LIGHTS_RANGE;
        } else {
            this.entityObject.light.range = 0;
        }
    }
}

VehicleEntity.prototype.enableAudio = function(value) {
    if (value !== this.audioEnabled) {
        this.audioEnabled = value;    
        if (this.entityObject.sound) {
            if (this.audioEnabled) {
                this.entityObject.sound.PlaySound();
            } else {
                this.entityObject.sound.StopSound();
            }
        }
    }
}

VehicleEntity.prototype.setGlowEffect = function(value) {
    if (value !== this.glowEffectEnabled) {
        this.glowEffectEnabled = value;    
        if (this.entityObject.mesh) {
            if (this.glowEffectEnabled) {
                var materialReferences = new AssetReferenceList();
                materialReferences = ["bora_body_green_glow.material", "bora_wheel.material", "bora_tyre.material", "bora_turnlght.material", "bora_rearlght.material", "bora_plastic.material", "bora_nplate.material","bora_mirror.material","bora_interior.material","bora_inter_w.material","bora_glassblk.material","bora_glass.material","bora_fronlght.material","bora_chrome.material","bora_braklght.material","bora_black.material","bora_backlght.material","bora_aluminium.material"];                        
                this.entityObject.mesh.meshMaterial = materialReferences;
            } else {
                this.entityObject.mesh.meshMaterial = this.orginalMeshMaterial;
            }
        }
    }
}

VehicleEntity.prototype.createVehicleEntity = function(name) {
    //var entityObject = this.scene.CreateEntity(this.scene.NextFreeId(), ["EC_Name", "EC_Placeable", "EC_Mesh", "EC_DynamicComponent", "EC_Sound", "EC_RigidBody", "EC_ParticleSystem", "Light"]);
    //var entityObject = this.scene.CreateEntity(this.scene.NextFreeId(), ["EC_Name", "EC_Placeable", "EC_Mesh", "EC_DynamicComponent", "EC_Sound", "EC_ParticleSystem", "Light"]);
    //var entityObject = this.scene.CreateEntity(this.scene.NextFreeId(), ["EC_Name", "EC_Placeable", "EC_Mesh", "EC_DynamicComponent", "EC_ParticleSystem", "Light"]);
    //var entityObject = this.scene.CreateEntity(this.scene.NextFreeId(), ["EC_Name", "EC_Placeable", "EC_Mesh", "EC_DynamicComponent", "EC_ParticleSystem"]);
    var entityObject = this.scene.CreateEntity(this.scene.NextFreeId(), ["EC_Name", "EC_Placeable", "EC_Mesh", "EC_DynamicComponent", "EC_ParticleSystem", "EC_Sound"]);    

    entityObject.name = name;
    entityObject.SetTemporary(true);     
    this.registerMousePressedAction(entityObject);
    
    this.initPlaceableComponent(entityObject);
    this.initMeshComponent(entityObject);
    //this.initRigidbodyComponent(entityObject);
    this.initParticleSystemComponent(entityObject);
    if (LIGHT_COMPONENT_ENABLED) {
        var lightComponent = entityObject.CreateComponent("EC_Light", "light");
        entityObject.AddComponent(lightComponent);
        this.initLightComponent(entityObject);
    }
    this.initSoundComponent(entityObject);

    return entityObject;
}

VehicleEntity.prototype.initLightComponent = function(entityObject) {
    entityObject.light.type = 1;
    entityObject.light.brightness = HEAD_LIGHTS_BRIGHTNESS;
    entityObject.light.constAtten = 1;
    entityObject.light.range = HEAD_LIGHTS_RANGE;
    entityObject.light.diffColor = new Color(1,1,1);
    entityObject.light.specColor = new Color(1,1,1);
    entityObject.light.castShadows = false;
    entityObject.light.innerAngle = HEAD_LIGHTS_INNER_ANGLE;
    entityObject.light.outerAngle = HEAD_LIGHTS_OUTER_ANGLE;
}

VehicleEntity.prototype.initPlaceableComponent = function(entityObject) {
    var transform = entityObject.placeable.transform;
    
    // position
    transform.pos.x = 0;
    transform.pos.y = 0;
    transform.pos.z = 0;

    // rotation
    transform.rot.x = 0; // the vehicle direction
    transform.rot.y = 0; // the vehicle direction
    transform.rot.y = 0; // the vehicle direction

    // scale
    transform.scale.x = 1;
    transform.scale.y = 1;
    transform.scale.z = 1;
    
    if (this.vehicleType == "T90") {
        transform.scale.x = 0.01;
        transform.scale.y = 0.01;
        transform.scale.z = 0.01;
    }
    entityObject.placeable.transform = transform;   
}

VehicleEntity.prototype.initParticleSystemComponent = function(entityObject) {
    entityObject.particlesystem.particleRef = "smoke.particle";
    entityObject.particlesystem.enabled = false;
}

VehicleEntity.prototype.initRigidbodyComponent = function(entityObject) {
    // physics
    //console.LogInfo(JSON.stringify(vehicleEntity.rigidbody));
    entityObject.rigidbody.shapeType = 1;
    entityObject.rigidbody.mass = VEHICLE_MASS;
    entityObject.rigidbody.useGravity = true;
    entityObject.rigidbody.size = float3(1.0, 1.0, 1.0);
    if (this.vehicleType == "T90") {
        entityObject.rigidbody.size = float3(100, 100, 100);
    }
    entityObject.rigidbody.collisionLayer = VEHICLE_RIGID_BODY_LAYER; 
    entityObject.rigidbody.collisionMask = 255 - VEHICLE_RIGID_BODY_LAYER;
    
    var physicsWorld = entityObject.rigidbody.World();
    var scene = entityObject.ParentScene();
    console.LogWarning("DEBUG "+JSON.stringify(scene.physics));
}

VehicleEntity.prototype.initMeshComponent = function(entityObject) {
    // mesh and material
    entityObject.mesh.meshRef = "bora.mesh";
    if (this.vehicleType == "gray-bora") {
        var materialReferences = new AssetReferenceList();
        materialReferences = ["bora_body.material", "bora_wheel.material", "bora_tyre.material", "bora_turnlght.material", "bora_rearlght.material", "bora_plastic.material", "bora_nplate.material","bora_mirror.material","bora_interior.material","bora_inter_w.material","bora_glassblk.material","bora_glass.material","bora_fronlght.material","bora_chrome.material","bora_braklght.material","bora_black.material","bora_backlght.material","bora_aluminium.material"];        
        entityObject.mesh.meshMaterial = materialReferences;
    } else if (this.vehicleType == "yellow-bora") {
        var materialReferences = new AssetReferenceList();
        materialReferences = ["bora_body_yeallow.material", "bora_wheel.material", "bora_tyre.material", "bora_turnlght.material", "bora_rearlght.material", "bora_plastic.material", "bora_nplate.material","bora_mirror.material","bora_interior.material","bora_inter_w.material","bora_glassblk.material","bora_glass.material","bora_fronlght.material","bora_chrome.material","bora_braklght.material","bora_black.material","bora_backlght.material","bora_aluminium.material"];                
        entityObject.mesh.meshMaterial = materialReferences;
    } else {
        // default vehicle
        var materialReferences = new AssetReferenceList();
        //materialReferences = ["bora_body_blue.material", "bora_wheel.material", "bora_tyre.material", "bora_turnlght.material", "bora_rearlght.material", "bora_plastic.material", "bora_nplate.material","bora_mirror.material","bora_interior.material","bora_inter_w.material","bora_glassblk.material","bora_glass.material","bora_fronlght.material","bora_chrome.material","bora_braklght.material","bora_black.material","bora_backlght.material","bora_aluminium.material"];                        
        materialReferences = ["bora_body_red.material", "bora_wheel.material", "bora_tyre.material", "bora_turnlght.material", "bora_rearlght.material", "bora_plastic.material", "bora_nplate.material","bora_mirror.material","bora_interior.material","bora_inter_w.material","bora_glassblk.material","bora_glass.material","bora_fronlght.material","bora_chrome.material","bora_braklght.material","bora_black.material","bora_backlght.material","bora_aluminium.material"];                                
        entityObject.mesh.meshMaterial = materialReferences;
    }
    
    this.orginalMeshMaterial = entityObject.mesh.meshMaterial;
    
    // Adjust mesh transformation
    var meshTransformation = entityObject.mesh.nodeTransformation;
    
    meshTransformation.pos.y = -0.5;
    
/*    if (this.vehicleType == "bora") {
        
    }    
    
    if (this.vehicleType == "taxi") {
        meshTransformation.pos.y = 1.0;
        meshTransformation.pos.y = -0.5;
    }*/
    
    entityObject.mesh.nodeTransformation = meshTransformation;
}

VehicleEntity.prototype.enableT90Mode = function() {
    this.vehicleType = "T90";
    
    this.entityObject.mesh.meshRef = "T90.mesh";
    var materialReferences = new AssetReferenceList();
    materialReferences = ["Main_Body.material","Gun.material","Turret.material","Main_Body_2.material"];
    this.entityObject.mesh.meshMaterial = materialReferences;    
    
    var meshTransformation = this.entityObject.mesh.nodeTransformation;
    meshTransformation.pos.y = 0.3;    
    
    meshTransformation.rot.x = 90;
    meshTransformation.rot.y = 180;
    meshTransformation.rot.z = 0;

    meshTransformation.scale.x = 0.01;
    meshTransformation.scale.y = 0.01;
    meshTransformation.scale.z = 0.01;
    
    this.entityObject.mesh.nodeTransformation = meshTransformation;    
}

VehicleEntity.prototype.initSoundComponent = function(entityObject) {
    entityObject.sound.soundRef = SOUND_FILE_NAME; 
    entityObject.sound.soundOuterRadius = OUTER_SOUND_RADIUS_MAX;
    entityObject.sound.soundInnerRadius = INNER_SOUND_RADIUS;
    entityObject.sound.soundGain = 1;
    entityObject.sound.spatial = true;
    entityObject.sound.loopSound = true;
    entityObject.sound.playOnLoad = false;
}

VehicleEntity.prototype.registerMousePressedAction = function(entityObject) {
    var self = this;
    entityObject.Action("MousePress").Triggered.connect(function() {
        self.handleClick();}
    );
}

VehicleEntity.prototype.handleClick = function() {
    this.setWorldCameraToFollowMode(true);
}

VehicleEntity.prototype.setWorldCameraToFollowMode = function(enabled) {
    var worldCameraEntity = this.scene.EntityByName(GLOBAL_WORLD_CAMERA_ENTITY_OBJECT_NAME);
    if (worldCameraEntity) {
        if (enabled) {
            worldCameraEntity.dynamiccomponent.SetAttribute("followVehicle", this.entityObject.id);
        } else {
            var currentFollowedId = worldCameraEntity.dynamiccomponent.GetAttribute("followVehicle","");
            if (currentFollowedId == this.entityObject.id) {
                worldCameraEntity.dynamiccomponent.SetAttribute("followVehicle", "");
            }
        }
    } else {
        console.LogWarning(">> VehicleEntity: Cannot set camera to follow mode, cannot find world camera object!");
    }
}
