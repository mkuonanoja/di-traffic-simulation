// (c) Matti Kuonanoja 2014 

engine.IncludeFile("TrafficModule/MathTools.js")

var MAX_INITIAL_SPEED = 20;
var TARGET_REACH_RANGE = 4.0;

var CORNER_CHECK_COUNT_FOR_CURVES = 1;
var CURVE_CHECK_RANGE = 20;
var CURVE_CHECK_MIN_ANGLE = 30;

var WAYPOINT_CHECK_COUNT_FOR_PAUSES = 1;
var PAUSE_CHECK_RANGE = 20;

function RouteNavigator(route) {
    this.route = route;

    this.currentTime = undefined;
    this.previousPosition = undefined;
    this.currentPosition = undefined;
    this.targetPosition = undefined;
    this.targetTime = undefined;
    this.targetWaypointIndex = undefined;
    
    this.routeLeft = route.getLength();
    this.travelledSegmentsLength = 0;
    this.pauseTime = 0;
}

RouteNavigator.prototype.getInitialPosition = function(simulationTime) {
    if (!this.route || !simulationTime) {
        return undefined;
    }
    
    var targetWaypointIndex = this.getInitialWaypointIndex(simulationTime);
    
    if (targetWaypointIndex == 0) {
        targetWaypointIndex += 1; // we get speed of the next segment
    }
    var previousWaypointIndex = targetWaypointIndex - 1;
    
    var targetLocation = this.route.getLocation(targetWaypointIndex);
    var previousLocation = this.route.getLocation(previousWaypointIndex);

    var targetPosition = targetLocation.getSimulationPosition();    
    var previousPosition = previousLocation.getSimulationPosition();
    
    var deltaX = targetPosition.x - previousPosition.x;
    var deltaY = targetPosition.y - previousPosition.y;
    
    var deltaTime = this.route.getTimestamp(targetWaypointIndex) - this.route.getTimestamp(previousWaypointIndex);    
    if (deltaTime == 0) {
        return targetPosition;
    }
    
    var timePassed = simulationTime - this.route.getTimestamp(previousWaypointIndex);
    var initProgress = timePassed / deltaTime

    if (this.getInitialPauseTimeLeftFromPreviosWaypoint(simulationTime) > 0) {
        initProgress = 0;
    }
    
    var initPosX = previousPosition.x + deltaX * initProgress;
    var initPosY = previousPosition.y + deltaY * initProgress;
    
    return { x: initPosX, y: initPosY };
}

RouteNavigator.prototype.getInitialSpeed = function(simulationTime) {
    if (!this.route || !simulationTime) {
        return undefined;
    }
    var targetWaypointIndex = this.getInitialWaypointIndex(simulationTime);

    if (this.getInitialPauseTimeLeftFromPreviosWaypoint(simulationTime) > 0) {
        return 0;
    }

    if (targetWaypointIndex == 0) {
        targetWaypointIndex += 1;
    }

    var previousWaypointIndex = targetWaypointIndex - 1;

    var targetLocation = this.route.getLocation(targetWaypointIndex);
    var previousLocation = this.route.getLocation(previousWaypointIndex);

    var targetPosition = targetLocation.getSimulationPosition();    
    var previousPosition = previousLocation.getSimulationPosition();
    
    var length = distanceTo(previousPosition.x, previousPosition.y, targetPosition.x, targetPosition.y);
    var deltaTime = this.route.getTimestamp(targetWaypointIndex) - this.route.getTimestamp(previousWaypointIndex);

    var speed = undefined;
    if (deltaTime > 0) {
        speed = length/deltaTime;
        speed = Math.min(speed, MAX_INITIAL_SPEED);
    } else {
        console.LogWarning(">> RouteNavigator: Initial speed to max value.");
        speed = MAX_INITIAL_SPEED
    }
    return speed;
}

RouteNavigator.prototype.getInitialDirection = function(simulationTime) {
    if (!this.route || !simulationTime) {
        console.LogWarning(">> RouteNavigator:Cannot get initial direction.");
        return undefined;
    }
    var targetWaypointIndex = this.getInitialWaypointIndex(simulationTime);
    if (targetWaypointIndex == 0) {
        targetWaypointIndex += 1;
    }
    if (this.getInitialPauseTimeLeftFromPreviosWaypoint(simulationTime) > 0) {
        targetWaypointIndex -= 1;
    }
    
    var previousWaypointIndex = targetWaypointIndex - 1;    

    var targetLocation = this.route.getLocation(targetWaypointIndex);
    var previousLocation = this.route.getLocation(previousWaypointIndex);

    var targetPosition = targetLocation.getSimulationPosition();
    var previousPosition = previousLocation.getSimulationPosition();
    

    var directionInRadians = directionTo(previousPosition.x, previousPosition.y, targetPosition.x, targetPosition.y)
    return radiansToDegrees(directionInRadians);
}

RouteNavigator.prototype.getInitialPauseTimeLeftFromPreviosWaypoint = function(simulationTime) {
    if (!this.route || !simulationTime) {
        console.LogWarning(">> RouteNavigator:Cannot get initial direction.");
        return undefined;
    }
    var targetWaypointIndex = this.getInitialWaypointIndex(simulationTime);
    if (targetWaypointIndex == 0) {
        targetWaypointIndex += 1;
    }
    var previousWaypointIndex = targetWaypointIndex - 1;    
    
    pauseTimeLeftFromPreviousWaypoint = 0;    
    if (previousWaypointIndex >= 0 && this.route.getPauseTime(previousWaypointIndex) > 0) {
        timeFromPreviousWaypoint = simulationTime - this.route.getTimestamp(previousWaypointIndex);
        pauseTimeLeftFromPreviousWaypoint = this.route.getPauseTime(previousWaypointIndex) - timeFromPreviousWaypoint;
        if (pauseTimeLeftFromPreviousWaypoint < 0 ) {
            pauseTimeLeftFromPreviousWaypoint = 0; 
        }
    }
    
    return pauseTimeLeftFromPreviousWaypoint;
}

RouteNavigator.prototype.setInitPauseTime = function(simulationTime) {
    this.pauseTime = this.getInitialPauseTimeLeftFromPreviosWaypoint(simulationTime);
}

RouteNavigator.prototype.update = function(deltaTime) {
    if (!this.targetWaypointIndex) {
        this.updateTargetWaypointIndex();
    }
    if (!this.targetPosition) {
        this.updateTargetPosition();
    }
    
    if (this.pauseTime > 0) {
        this.updateTargetDirection();    
        this.consumePauseTime(deltaTime);
        //console.LogWarning("DEBUG: pause time left "+this.pauseTime)
        return;
    }

    if (this.isTargetReached() || this.isTargetBypassed()) {
        this.updatePauseTime();
        this.updateTargetWaypointIndex();
        this.updateTargetPosition();
        this.updateTargetTime();
        this.updateTravelledSegmentsLength();
        this.updatePreviousPosition();
    }
    
    this.updateTargetDirection();            
}

RouteNavigator.prototype.updatePauseTime = function() {
    var waypointPauseTime = this.route.getPauseTime(this.targetWaypointIndex);
    if (waypointPauseTime > 0) {
        this.pauseTime = waypointPauseTime;
        //console.LogInfo(">> RouteNavigator: Pause time set"+waypointPauseTime+" ("+this.targetPosition.x+" "+this.targetPosition.y+")");
    }
}

RouteNavigator.prototype.getRouteLeft = function() {
    if (this.previousPosition) {
        var distanceToPreviousWaypoint = distanceTo(this.currentPosition.x, this.currentPosition.y, this.previousPosition.x, this.previousPosition.y)
        var left = this.route.getLength() - this.travelledSegmentsLength - distanceToPreviousWaypoint;
        return left;
    } else {
        return undefined;
    }
}

RouteNavigator.prototype.getWaypointsLeftCount = function() {
    var waypointsLeft = this.route.getWaypointCount() - this.targetWaypointIndex;
    return waypointsLeft;
}

RouteNavigator.prototype.consumePauseTime = function(deltaTime) {
    if (this.pauseTime > deltaTime) {
        this.pauseTime -= deltaTime;
//        console.LogInfo(">> RouteNavigator: Pause time left:"+this.pauseTime+")");
    } else {
        this.pauseTime = 0;
//        console.LogInfo(">> RouteNavigator: Pause time consumed ("+this.targetPosition.x+" "+this.targetPosition.y+")");
    }
}

RouteNavigator.prototype.getTargetPosition = function() {
    return this.targetPosition;
}

RouteNavigator.prototype.getTargetTime = function() {
    return this.targetTime;
}

RouteNavigator.prototype.setCurrentPosition = function(position) {
    this.currentPosition = position;
}

RouteNavigator.prototype.setCurrentTime = function(timestamp) {
    this.currentTime = timestamp;
}

RouteNavigator.prototype.isConsumingPauseTime = function() {
    if (this.pauseTime > 0) {
        return true;
    } else {
        return false;
    }
}

RouteNavigator.prototype.isRouteCompleted = function() {
    if (this.targetWaypointIndex == this.route.waypoints.length && this.isTargetReached()) {
        return true;
    } else {
        return false;
    }
}

RouteNavigator.prototype.isTargetReached = function() {
    if (!this.targetPosition) {
        this.updateTargetPosition();
    }
    var distanceToTarget = distanceTo(this.currentPosition.x, this.currentPosition.y, this.targetPosition.x, this.targetPosition.y);
    if (distanceToTarget < TARGET_REACH_RANGE) {
        return true;
    } else {
        return false;
    }
}

RouteNavigator.prototype.updateTargetDirection = function() {   
    if (!this.currentPosition || !this.targetPosition)  {
        console.LogError(">> RouteNavigator: Cannot update target direction, no position information available!");
        return undefined;
    }
    if (this.isRouteCompleted()) {
        return;
    }
    var targetDicationInRadians = directionTo(this.currentPosition.x, this.currentPosition.y, this.targetPosition.x, this.targetPosition.y);
    this.targetDirection = normalizeDegrees(radiansToDegrees(targetDicationInRadians));
}

RouteNavigator.prototype.updateTargetWaypointIndex = function() {   
    if (!this.route) {
        return;
    }

    if (this.isRouteCompleted()) {
        return;
    }

    if (!this.targetWaypointIndex) {
        this.targetWaypointIndex = this.getInitialWaypointIndex(this.currentTime);
        this.updateTravelledSegmentsLength();
        this.updatePreviousPosition();
    } else {
        if (this.targetWaypointIndex < this.route.getWaypointCount() - 1) {
            this.targetWaypointIndex += 1;
        }
    }
}

RouteNavigator.prototype.updateTargetPosition = function() {   
    if (!this.route || !this.targetWaypointIndex) {
        console.LogWarning(">> RouteNavigator: Cannot update target position.");
        return;
    }

    var targetLocation = {latitude: this.route.getLatitude(this.targetWaypointIndex), longitude: this.route.getLongitude(this.targetWaypointIndex) };
    this.targetPosition = locationToSimulationPosition(targetLocation.latitude, targetLocation.longitude);    
}

RouteNavigator.prototype.updatePreviousPosition = function() {   
    if (this.targetWaypointIndex > 0) {
        var previousLocation = new Location(this.route.getLatitude(this.targetWaypointIndex - 1), this.route.getLongitude(this.targetWaypointIndex - 1) );
        this.previousPosition = previousLocation.getSimulationPosition();
    } else {
        this.previousPosition = undefined;
    }
}

RouteNavigator.prototype.updateTravelledSegmentsLength = function() {   
    var length = 0;
    for (var i = 0; i < this.targetWaypointIndex - 1; i++) {

        var segmentStartLocation = new Location(this.route.getLatitude(i), this.route.getLongitude(i) );
        var segmentStartPosition = segmentStartLocation.getSimulationPosition();

        var segmentEndLocation = new Location(this.route.getLatitude(i + 1), this.route.getLongitude(i + 1) );
        var segmentEndPosition = segmentEndLocation.getSimulationPosition();

        var segmentLength = distanceTo(segmentStartPosition.x, segmentStartPosition.y, segmentEndPosition.x, segmentEndPosition.y);
        length += segmentLength;
    }
    this.travelledSegmentsLength = length;
}

RouteNavigator.prototype.updateTargetTime = function() {   
    if (!this.route || !this.targetWaypointIndex) {
        return;
    }

    this.targetTime = this.route.getTimestamp(this.targetWaypointIndex);
}

RouteNavigator.prototype.getInitialWaypointIndex = function(timestamp) {
    if (!this.route) {
        return undefined;
    }
    if (!timestamp) {
        console.LogWarning(">> RouteNavigator: ERROR, timestamp not given");
        return undefined;
    }

    for (var i = 0; i < this.route.waypoints.length; i++) {
        if (i < this.route.waypoints.length - 1) {
            var segmentStartLocation = new Location(this.route.getLatitude(i), this.route.getLongitude(i));
            var segmentEndLocation = new Location(this.route.getLatitude(i+1), this.route.getLongitude(i+1));
            var segmentStartPosition = segmentStartLocation.getSimulationPosition(); 
            var segmentEndPosition = segmentEndLocation.getSimulationPosition();
            var segmentLength = distanceTo(segmentStartPosition.x, segmentStartPosition.y, segmentEndPosition.x, segmentEndPosition.y);
            this.routeLeft -= segmentLength;
        }
    
        var waypointTimestamp = this.route.getTimestamp(i);
        if (waypointTimestamp > timestamp) {
            return i;
        }
    }
    console.LogWarning(">> RouteNavigator: WARNING, route is already completed!");
    return this.route.waypoints.length - 1; 
}

RouteNavigator.prototype.isCurveComing = function(currentPosition) {
   for (var i = 0; i < CORNER_CHECK_COUNT_FOR_CURVES; i++) {
        var curvePosition = this.getNextCurvePosition(i);
        if (curvePosition) {
            var distanceToCurvePosition = distanceTo(currentPosition.x, currentPosition.y, curvePosition.x, curvePosition.y);
            if (distanceToCurvePosition < CURVE_CHECK_RANGE) {
                return true;
            }
        }
    }
    return false;    
}

RouteNavigator.prototype.isPauseComing = function(currentPosition) {
   for (var i = 0; i < WAYPOINT_CHECK_COUNT_FOR_PAUSES; i++) {
        var pausePosition = this.getNextPausePosition(i);
        if (pausePosition) {
            var distanceToPausePosition = distanceTo(currentPosition.x, currentPosition.y, pausePosition.x, pausePosition.y);
            if (distanceToPausePosition < PAUSE_CHECK_RANGE) {
                return true;
            }
        }
    }
    return false;    
}


RouteNavigator.prototype.getNextPausePosition = function(deltaIndex) {
    var targetIndex = this.targetWaypointIndex + deltaIndex;
    var previousTargetIndex = targetIndex - 1;
    var nextTargetIndex = targetIndex + 1;
    
    if (previousTargetIndex >= 0 && nextTargetIndex < this.route.getWaypointCount()) {
        if (this.route.getPauseTime(targetIndex) > 0) {
            var targetlocation = {latitude: this.route.getLatitude(targetIndex), longitude: this.route.getLongitude(targetIndex) };
            var targetPosition = locationToSimulationPosition(targetlocation.latitude, targetlocation.longitude);
            return targetPosition;
        }
    } else {
        return undefined;
    }
}

RouteNavigator.prototype.getNextCurvePosition = function(deltaIndex) {
    var targetIndex = this.targetWaypointIndex + deltaIndex;
    var previousTargetIndex = targetIndex - 1;
    var nextTargetIndex = targetIndex + 1;
    
    if (previousTargetIndex >= 0 && nextTargetIndex < this.route.getWaypointCount()) {
        var previousTargetLocation = {latitude: this.route.getLatitude(previousTargetIndex), longitude: this.route.getLongitude(previousTargetIndex) };
        var targetlocation = {latitude: this.route.getLatitude(targetIndex), longitude: this.route.getLongitude(targetIndex) };
        var nextTargetLocation = {latitude: this.route.getLatitude(nextTargetIndex), longitude: this.route.getLongitude(nextTargetIndex) };
        
        var previousTargetPosition = locationToSimulationPosition(previousTargetLocation.latitude, previousTargetLocation.longitude);
        var targetPosition = locationToSimulationPosition(targetlocation.latitude, targetlocation.longitude);
        var nextTargetPosition = locationToSimulationPosition(nextTargetLocation.latitude, nextTargetLocation.longitude);
        
        var enteringDirection = radiansToDegrees(directionTo(previousTargetPosition.x, previousTargetPosition.y, targetPosition.x, targetPosition.y));
        var leavingDirection =  radiansToDegrees(directionTo(targetPosition.x, targetPosition.y, nextTargetPosition.x, nextTargetPosition.y));
        var deltaAngle = getDeltaDirection(enteringDirection, leavingDirection);

        if (Math.abs(deltaAngle) > CURVE_CHECK_MIN_ANGLE) {
            return targetPosition;
        }
    } else {
        return undefined;
    }
}

RouteNavigator.prototype.isTargetBypassed = function() {
    // todo...
    return false;
}